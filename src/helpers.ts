export function renameKeys(keysMap: { [oldProp: string]: string }, obj: object) {
    return Object
        .keys(obj)
        .reduce((acc, key) => ({
            ...acc,
            ...{ [keysMap[key] || key]: obj[key] },
        }), {});
}

export function renameProp(oldProp: string, newProp: string, { [oldProp]: old, ...others }) {
    return { [newProp]: old, ...others };
}
