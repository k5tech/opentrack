import { writeFile, writeFileSync } from 'fs';
import * as http from 'http';
// console.log(new Date());
// const _GLOBAL: any = global;
// const mockDate = new Date('2018-12-12T17:04:46Z');
// _GLOBAL.Date = class {
//     public static now() {
//       return mockDate;
//     }

//     public valueOf() {
//       return mockDate;
//     }
//     public getTime() {
//         return mockDate;
//     }
//     public getUTCFullYear() {
//         return mockDate;
//     }
//     public getUTCMonth() {
//         return mockDate;
//     }
//     public toString() {
//         return mockDate;
//     }
//   };
// global.Date = mockRequestDate;
// global.Date.now = () => mockRequestDate.getTime();

import { OpenTrackCustomerAPI } from '../index';
import { OpenTrackPartsAPI } from '../opentrack.parts.api';
import { OpenTrackServiceAPI } from '../opentrack.service.api';

// console.log(new Date());

const examples = {
    customer: {
        customerAdd: [
            {
                name: 'request-1',
                params: {
                    FirstName: 'Peter',
                    LastName: 'Smith',
                },
            },
            {
                dataToken: 'example-uid',
                name: 'request-2',
                params: {
                    Address1: '55 Test Road',
                    BirthDate: '19810215',
                    BusinessPhone: '3333333333',
                    CellPhone: '2222222222',
                    City: 'Waterloo',
                    CustomerPreferredName: 'Cert',
                    CustomerType: 'R',
                    DriversLicense: '4F5ER4GF56R4F56',
                    Email1: 'andrewh@oneeightycorp.com',
                    FirstName: 'John',
                    Gender: 'M',
                    InternationalZipCode: 'N1W 2E3',
                    LastName: 'Smith',
                    PhoneNumber: '1111111111',
                    PreferredLanguage: 'ENG',
                    Salutation: 'Mr.',
                    StateCode: 'ON',
                    TypeCode: 'I',
                },
            },
        ],
        customerList: [
            {
                name: 'request-1',
                params: {
                    ZipCode: '97700',
                },
            },
            {
                name: 'request-2',
                params: {
                    ExcludeBlankAddress: 'Y',
                    ExcludeBlankEmail: 'Y',
                    ExcludeBlankPhone: 'Y',
                    IncludeCompanies: 'Y',
                    ZipCode: '84095',
                },
            },

        ],
        customerLookup: [
            {
                name: 'request-1',
                params: {
                    CustomerNumber: '1039358',
                },
            },
            {
                name: 'request-2',
                params: {
                    CustomerNumber: '1039359',
                },
            },

        ],
        customerSearch: [
            {
                name: 'request-1',
                params: {
                    Phone: '9287632328',
                },
            },
        ],
        customerUpdate: [
            {
                name: 'request-1',
                params: {
                    FirstName: 'Peter',
                    LastName: 'Smith',
                },
            },
            {
                name: 'request-2',
                params: {
                    Address1: '14975 S. CANYON ROAD',
                    AllowContactByEmail: 'Y',
                    AllowContactByPhone: 'Y',
                    AllowContactByPostal: 'Y',
                    BirthDate: '0',
                    BusinessExt: '234',
                    BusinessPhone: '5052342563',
                    BusinessPhoneExtension: '1234',
                    // CCCD: '0', // CCCD Used in example doc but not in request spec
                    CellPhone: '0',
                    City: 'SANTAQUIN',
                    CustomerNumber: '1038979',
                    Email1: 'TESTEMAIL1@TEST.COM',
                    Email2: 'TESTEMAIL2@TEST.COM',
                    ExternalCrossReferenceKey: '0',
                    FaxNumber: '0',
                    FirstName: 'AARON',
                    InternationalBusinessPhone: '5052342563',
                    InternationalHomePhone: '5052568745',
                    // LastChangeDate: '20140602', //  Used in example doc but not in request spec
                    LastName: 'JONES',
                    OtherPhone: '0',
                    PagePhone: '0',
                    PhoneNumber: '5052568745',
                    StateCode: 'UT',
                    TypeCode: 'I',
                    ZipCode: '87048',
                },
            },
        ],
    },
    service: {
        getClosedRepairOrders: [
            {
                name: 'request-1',
                params: {
                    CustomerNumber: 1039974,
                },
            },
        ],
        openRepairOrderLookup: [
            {
                name: 'request-1',
                params: {
                    CreatedDateTimeEnd: (new Date('2017-11-09T22:02:51Z')).toISOString(),
                    CreatedDateTimeStart: (new Date('2017-11-07T22:02:51Z')).toISOString(),
                    InternalOnly: 'Y',
                },
            },
        ],
    },
};
const port = 8_000;
const hostname = '127.0.0.1';
const location = `http://${hostname}:${port}`;
const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.end();
}).listen(port, hostname);
// const DATE = Date;

const username = 'username';
const password = 'password';
const enterpriseCode = 'ZE';
const companyNumber = 'ZE7';
const serverName = 'arkonap.arkona.com';

createApis().then(async (apis) => {
    for (const api of Object.keys(examples)) {
        console.log('Testing', api);
        await runExamples(examples[api], apis[api]);
    }
}).then(() => {
    server.close();
});

async function createApis(endpoint?: string) {
    const customer = (await OpenTrackCustomerAPI.create(endpoint))
        .authenticatedAs(username, password)
        .dealer(enterpriseCode, companyNumber, serverName);
    const service = (await OpenTrackServiceAPI.create(endpoint))
        .authenticatedAs(username, password)
        .dealer(enterpriseCode, companyNumber, serverName);
    // const parts = (await OpenTrackPartsAPI.create())
    //                 .authenticatedAs(username, password)
    //                 .dealer(enterpriseCode, companyNumber, serverName);
    return { customer, service };
}

interface ExampleSpec {
    name: string;
    params: { [key: string]: string | number };
    dataToken?: string;
}
interface ExampleSet {
    [method: string]: ExampleSpec[];
}
async function runExamples(exampleSet: ExampleSet, api: any) {
    const client = await api.soapClient();
    client.setEndpoint(location);
    for (const methodName of Object.keys(exampleSet)) {
        for (const test of exampleSet[methodName]) {
            const apiMethod = api[methodName];
            const exampleParams = test.params;
            const dataToken = test.dataToken;
            const outputFile = `test-${methodName}-${test.name}.xml`;
            if (typeof api[methodName] !== 'function') {
                // console.error(`${methodName} is not a method on supplied api`);
                throw Error(`${methodName} is not a method on supplied api`);
                // return;
            }
            // try {

            const destFile = `${__dirname}/example-requests/${outputFile}`;
            console.log(`Running: ${methodName} with params`, exampleParams);
            if (dataToken) {
                await api[methodName](dataToken, exampleParams);
            } else {
                await api[methodName](exampleParams);
            }
            const xml = client.lastRequest;
            writeFileSync(destFile, xml);
            // } catch (e) {
            //     console.error(e);
            // }
        }
    }
}
