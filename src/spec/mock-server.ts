// Import environment variables used for mock test requests
import 'dotenv/config';
import { existsSync, writeFileSync } from 'fs';
import * as http from 'http';
import * as soap from 'soap';
import { OpenTrackAPIBase } from '../opentrack.api';

export const mockRequestDate = new Date('2018-12-12T17:04:46Z');

export const mockAuth = {
    password: 'password',
    username: 'username',
};

export const mockDealer = {
    companyNumber: 'ZE7',
    enterpriseCode: 'ZE',
    serverName: 'arkonap.arkona.com',
};

/**
 * Flatten a multiline string and remove whitespace
 * @param content Contents of file
 */
export function normalizeFileContents(content: string) {
    return content.replace(/(\r\n\t|\n|\r\t)\s*/gm, '').replace(/\s/g, '');
}

export function createMockServer(xml: string, port = 8000) {
    const server = http.createServer((request, response) => {
        response.end(`404: Not Found: ${request.url}`);
    });

    server.listen(port);
    const soapServer = soap.listen(server, '/', null, xml);
    return { server, soapServer };
}

export function createSingleResponseServer(content: string, port = 8000) {
    const server = http.createServer((request, response) => {
        response.statusCode = 200;
        response.write(content);
        response.end();
    });
    server.listen(port);
    return { server };
}

export function mockAuthenticatedAPI<T extends OpenTrackAPIBase>(baseApi: T) {
    return baseApi.authenticatedAs(mockAuth.username, mockAuth.password)
        .dealer(mockDealer.enterpriseCode, mockDealer.companyNumber, mockDealer.serverName);
}

export async function testRequest<T extends OpenTrackAPIBase, K extends keyof T>(
    api: T, methodName: K, params: any, filePath = null, overwrite = false,
) {
    const username = process.env.OPENTRACK_USERNAME;
    const password = process.env.OPENTRACK_PASSWORD;

    const authenticatedApi = mockAuthenticatedAPI(api).authenticatedAs(username, password);
    // const authenticatedApi = api;
    const func = authenticatedApi[methodName];
    // if (typeof authenticatedApi[methodName] === 'function') {
    if (typeof func === 'function') {
        // const result = await authenticatedApi[methodName](params);
        const result = await func(params);
        if (filePath) {
            const lastResponse = authenticatedApi.lastResponseXML;
            if (!existsSync(filePath) || overwrite) {
                writeResponseXML(filePath, lastResponse);
            } else {
                console.log(`File: [${filePath}] already exists`);
            }
        }
        return result;
    }
    throw new Error(`Method: [${methodName}] is not defined on api class.`);
}

function writeResponseXML(filePath: string, content: any) {
    return writeFileSync(filePath, content, 'utf8');
}
