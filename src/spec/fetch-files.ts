import * as fs from 'fs';
import * as https from 'https';

/**
 * Download WSDL files for testing
 */

const urls = {
    customer: 'https://otstaging.arkona.com/opentrack/customerapi.asmx?WSDL',
    parts: 'https://otstaging.arkona.com/opentrack/partsapi.asmx?WSDL',
    service: 'https://otstaging.arkona.com/opentrack/serviceapi.asmx?WSDL',
};

function updateFiles(urlObject: {[key: string]: string}) {
    for (const key in urlObject) {
        if (urls.hasOwnProperty(key)) {
            const destPath = `${__dirname}/${key}.wsdl`;
            const dest = fs.createWriteStream(destPath);
            const url = urlObject[key];
            https.get(url, (response) => {
                response.pipe(dest);
            });
        }
    }
}

updateFiles(urls);
