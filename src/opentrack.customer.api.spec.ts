import { readFileSync } from 'fs';
import { Server } from 'http';
import { Server as SoapServer } from 'soap';
import * as convert from 'xml-js';
import { OpenTrackCustomerAPI } from './opentrack.customer.api';
import { createMockServer, mockAuth, mockDealer, mockRequestDate, normalizeFileContents, createSingleResponseServer, mockAuthenticatedAPI, testRequest } from './spec/mock-server';

describe('OpenTrack Customer API', () => {
    const port = 8_000;
    let api: OpenTrackCustomerAPI;
    let server: Server;
    let soapServer: SoapServer;
    const endpoint = `http://localhost:${port}`;
    describe('requests', () => {

        beforeEach(async () => {
            const location = `http://localhost:${port}`;

            const xml = readFileSync(`${__dirname}/spec/customer.wsdl`, 'utf8')
                .replace(
                    '<soap:address location="https://otstaging.arkona.com/opentrack/customerapi.asmx" />',
                    `<soap:address location="${location}" />`);

            const serverConfig = createMockServer(xml);

            server = serverConfig.server;
            soapServer = serverConfig.soapServer;

            api = (await OpenTrackCustomerAPI.create(location));
        });

        afterEach((done) => {
            jasmine.clock().uninstall();
            server.close(() => {
                server = null;
                soapServer = null;
                done();
            });
        });

        describe('client configuration', () => {
            it('sets authentication information', async () => {
                const requestSpy = jasmine.createSpy('request');
                soapServer.on('request', requestSpy);

                const client = api.authenticatedAs('username-abc', 'password-123');
                await client.customerSearch({ FirstName: 'Han' });

                expect(requestSpy).toHaveBeenCalledWith(
                    jasmine.objectContaining({
                        Header: {
                            Security: jasmine.objectContaining({
                                UsernameToken: jasmine.objectContaining({
                                    Password: jasmine.objectContaining({
                                        $value: 'password-123',
                                    }),
                                    Username: 'username-abc',
                                }),
                            }),
                        },
                    }),
                    'CustomerSearch');
            });

            it('sets dealer information', async () => {
                const requestSpy = jasmine.createSpy('request');
                soapServer.on('request', requestSpy);

                const client = api.dealer('ent-code', 'comp-num', 'serv-name');
                await client.customerSearch({ FirstName: 'Han' });

                expect(requestSpy).toHaveBeenCalledWith(
                    jasmine.objectContaining({
                        Body: {
                            CustomerSearch: jasmine.objectContaining({
                                Dealer: jasmine.objectContaining({
                                    CompanyNumber: 'comp-num',
                                    EnterpriseCode: 'ent-code',
                                    ServerName: 'serv-name',
                                }),
                            }),
                        },
                    }),
                    'CustomerSearch');
            });

            xit('handles a error response', async () => {
                const errorResponseFilename = 'error.cs01.xml';
            });

            // it('has expected soap methods', async () => {
            //     const description = await api.describeDetail();
            //     const expectedMethods = ['CustomerLookup', 'CustomerSearch', 'CustomerUpdate', 'CustomerAdd',
            //                              'CustomerList'];

            //     expect(description).toBeDefined();
            //     for (const expectedMethod of expectedMethods) {
            //         expect(description[expectedMethod])
            //             .toBeDefined(`Method: ${expectedMethod} was not found in soap description`);
            //     }

            // });

        });

        describe('customer add', () => {
            const expectedMethod = 'CustomerAdd';

            it('should have a soap description', async () => {
                const description = await api.describeDetail();
                expect(description).toBeDefined();
                expect(description[expectedMethod])
                    .toBeDefined(`Method: ${expectedMethod} was not found in soap description`);
            });

            it('should add a customer', async () => {
                const requestSpy = jasmine.createSpy('request');
                soapServer.on('request', requestSpy);
                const customer = {
                    FirstName: 'Brandon',
                    LastName: 'Johnson',
                    TypeCode: 'I',
                };
                const searchToken = 'data-token-from-search';

                await api.customerAdd(searchToken, customer);

                expect(requestSpy).toHaveBeenCalledWith(...searchBody(expectedMethod, {
                    Customer: {
                        DataToken: 'data-token-from-search',
                        FirstName: 'Brandon',
                        LastName: 'Johnson',
                        TypeCode: 'I',
                    },
                }));
            });

            it('should request with expected xml', async () => {
                let request: string;
                const authenticatedApi = mockAuthenticatedApi(api);
                const filename = 'customer-add.request-1-formatted.xml';
                const expectedXML = readFileSync(`${__dirname}/spec/example-requests/customer/${filename}`, 'utf8');

                soapServer.log = (type: string, data) => {
                    if (type === 'received') {
                        request = data;
                    }
                };
                const customer = {
                    FirstName: 'Dealertrack',
                    LastName: 'Cert',
                    TypeCode: 'I',
                };
                const searchToken = '38JseT70Rl376XHHpOWkfn5ybs6l5Decee8QzP0=';
                jasmine.clock().mockDate(mockRequestDate);

                await authenticatedApi.customerAdd(searchToken, customer);

                expect(normalizeFileContents(request)).toBe(normalizeFileContents(expectedXML));
            });

            it('should request with expected xml example 2', async () => {
                let request: string;
                const authenticatedApi = mockAuthenticatedApi(api);
                const filename = 'customer-add.request-2-formatted.xml';
                const expectedXML = readFileSync(`${__dirname}/spec/example-requests/customer/${filename}`, 'utf8');

                soapServer.log = (type: string, data) => {
                    if (type === 'received') {
                        request = data;
                    }
                };
                const customer = {
                    Address1: '55 Test Road',
                    BirthDate: '19810215',
                    BusinessPhone: '3333333333',
                    CellPhone: '2222222222',
                    City: 'Waterloo',
                    CustomerPreferredName: 'Cert',
                    CustomerType: 'R',
                    DriversLicense: '4F5ER4GF56R4F56',
                    Email1: 'andrewh@oneeightycorp.com',
                    FirstName: 'John',
                    Gender: 'M',
                    InternationalZipCode: 'N1W 2E3',
                    LastName: 'Smith',
                    PhoneNumber: '1111111111',
                    PreferredLanguage: 'ENG',
                    Salutation: 'Mr.',
                    StateCode: 'ON',
                    TypeCode: 'I',
                } as any;
                const searchToken = 'uid-string-abc123';
                jasmine.clock().mockDate(mockRequestDate);

                await authenticatedApi.customerAdd(searchToken, customer);

                xmlDiffCheck(request, expectedXML);
            });
        });

        describe('customer search', () => {
            const expectedMethod = 'CustomerSearch';

            it('should have a soap description', async () => {
                const description = await api.describeDetail();
                expect(description).toBeDefined();
                expect(description[expectedMethod])
                    .toBeDefined(`Method: ${expectedMethod} was not found in soap description`);
            });

            it('should search by phone number', async () => {
                const requestSpy = jasmine.createSpy('request');
                soapServer.on('request', requestSpy);

                await api.customerSearch({ Phone: '9287632328' });

                expect(requestSpy).toHaveBeenCalledWith(...searchBody(expectedMethod, {
                    SearchParms: {
                        Phone: '9287632328',
                    },
                }));
            });

            it('should search by phone number (E.164 Format)', async () => {
                const requestSpy = jasmine.createSpy('request');
                soapServer.on('request', requestSpy);

                await api.customerSearch({ Phone: '+19287632328' });

                expect(requestSpy).toHaveBeenCalledWith(...searchBody(expectedMethod, {
                    SearchParms: {
                        Phone: '9287632328',
                    },
                }));

            });

            it('should request with expected xml', async () => {
                let request: string;
                const authenticatedApi = mockAuthenticatedApi(api);
                // const filename = 'customer-search.request-1.xml';
                const filename = 'customer-search.request-1-formatted.xml';
                const expectedXML = readFileSync(`${__dirname}/spec/example-requests/customer/${filename}`, 'utf8');

                soapServer.log = (type: string, data) => {
                    if (type === 'received') {
                        request = data;
                    }
                };
                jasmine.clock().mockDate(mockRequestDate);

                await authenticatedApi.customerSearch({ Phone: '9287632328' });

                expect(normalizeFileContents(request)).toBe(normalizeFileContents(expectedXML));
            });

            it('should request with expected xml request 2', async () => {
                let request: string;
                const authenticatedApi = mockAuthenticatedApi(api);
                const filename = 'customer-search.request-2-formatted.xml';
                const expectedXML = readFileSync(`${__dirname}/spec/example-requests/customer/${filename}`, 'utf8');

                soapServer.log = (type: string, data) => {
                    if (type === 'received') {
                        request = data;
                    }
                };
                jasmine.clock().mockDate(mockRequestDate);

                await authenticatedApi.customerSearch({ LastName: 'RETAIL' });

                expect(normalizeFileContents(request)).toBe(normalizeFileContents(expectedXML));
            });
        });

        describe('customer lookup', () => {
            it('should lookup by customer number', async () => {
                const requestSpy = jasmine.createSpy('request');
                soapServer.on('request', requestSpy);

                await api.customerLookup({ CustomerNumber: '1039358' });

                expect(requestSpy).toHaveBeenCalledWith(...searchBody('CustomerLookup', {
                    CustomerNumber: '1039358',
                }));
            });

            it('should request with expected xml', async () => {
                let request: string;
                const authenticatedApi = mockAuthenticatedApi(api);
                const filename = 'customer-lookup.request-1-formatted.xml';
                const expectedXML = readFileSync(`${__dirname}/spec/example-requests/customer/${filename}`, 'utf8');

                soapServer.log = (type: string, data) => {
                    if (type === 'received') {
                        request = data;
                    }
                };
                jasmine.clock().mockDate(mockRequestDate);

                await authenticatedApi.customerLookup({ CustomerNumber: '1039358' });

                expect(normalizeFileContents(request)).toBe(normalizeFileContents(expectedXML));
            });

            it('should request with expected xml request 2', async () => {
                let request: string;
                const authenticatedApi = mockAuthenticatedApi(api);
                const filename = 'customer-lookup.request-2-formatted.xml';
                const expectedXML = readFileSync(`${__dirname}/spec/example-requests/customer/${filename}`, 'utf8');

                soapServer.log = (type: string, data) => {
                    if (type === 'received') {
                        request = data;
                    }
                };
                jasmine.clock().mockDate(mockRequestDate);

                await authenticatedApi.customerLookup({
                    CommonCustomerID: '667',
                    CommonCustomerVersion: '0',
                    CustomerNumber: '151635',
                });

                expect(normalizeFileContents(request)).toBe(normalizeFileContents(expectedXML));
            });
        });

        describe('customer list', () => {
            it('should lookup by zip code', async () => {
                const requestSpy = jasmine.createSpy('request');
                soapServer.on('request', requestSpy);

                await api.customerList({ ZipCode: '97700' });

                expect(requestSpy).toHaveBeenCalledWith(...searchBody('CustomerList', {
                    ListParms: {
                        ZipCode: '97700',
                    },
                }));
            });

            it('should request with expected xml', async () => {
                let request: string;
                const authenticatedApi = mockAuthenticatedApi(api);
                const filename = 'customer-list.request-1-formatted.xml';
                const expectedXML = readFileSync(`${__dirname}/spec/example-requests/customer/${filename}`, 'utf8');

                soapServer.log = (type: string, data) => {
                    if (type === 'received') {
                        request = data;
                    }
                };
                jasmine.clock().mockDate(mockRequestDate);

                await authenticatedApi.customerList({ ZipCode: '97700' });

                expect(normalizeFileContents(request)).toBe(normalizeFileContents(expectedXML));
            });

            it('should request with expected xml request 2', async () => {
                let request: string;
                const authenticatedApi = mockAuthenticatedApi(api);
                const filename = 'customer-list.request-2-formatted.xml';
                const expectedXML = readFileSync(`${__dirname}/spec/example-requests/customer/${filename}`, 'utf8');

                soapServer.log = (type: string, data) => {
                    if (type === 'received') {
                        request = data;
                    }
                };
                jasmine.clock().mockDate(mockRequestDate);

                await authenticatedApi.customerList({
                    ExcludeBlankAddress: 'Y',
                    ExcludeBlankEmail: 'Y',
                    ExcludeBlankPhone: 'Y',
                    IncludeCompanies: 'Y',
                    ZipCode: '84095',
                });

                xmlDiffCheck(request, expectedXML);
            });
        });

        describe('customer update', () => {
            it('should format request', async () => {
                const requestSpy = jasmine.createSpy('request');
                soapServer.on('request', requestSpy);

                const customer = {
                    CustomerNumber: '007',
                    FirstName: 'Dealertrack',
                    LastName: 'Cert',
                    TypeCode: 'I',
                };
                const searchToken = 'token';

                await api.customerUpdate(customer);

                expect(requestSpy).toHaveBeenCalledWith(...searchBody('CustomerUpdate', {
                    Customer: {
                        CustomerNumber: '007',
                        FirstName: 'Dealertrack',
                        LastName: 'Cert',
                        TypeCode: 'I',
                    },
                }));
            });

            it('should request with expected xml', async () => {
                let request: string;
                const authenticatedApi = mockAuthenticatedApi(api);
                const filename = 'customer-update.request-1-formatted.xml';
                const expectedXML = readFileSync(`${__dirname}/spec/example-requests/customer/${filename}`, 'utf8');

                soapServer.log = (type: string, data) => {
                    if (type === 'received') {
                        request = data;
                    }
                };
                jasmine.clock().mockDate(mockRequestDate);

                await authenticatedApi.customerUpdate({
                    CustomerNumber: '007',
                    FirstName: 'Dealertrack',
                    LastName: 'Cert',
                    TypeCode: 'I',
                });

                expect(normalizeFileContents(request)).toBe(normalizeFileContents(expectedXML));
            });

            it('should request with expected xml request 2', async () => {
                let request: string;
                const authenticatedApi = mockAuthenticatedApi(api);
                const filename = 'customer-update.request-2-formatted.xml';
                const expectedXML = readFileSync(`${__dirname}/spec/example-requests/customer/${filename}`, 'utf8');

                soapServer.log = (type: string, data) => {
                    if (type === 'received') {
                        request = data;
                    }
                };
                jasmine.clock().mockDate(mockRequestDate);

                await authenticatedApi.customerUpdate({
                    Address1: '14975 S. CANYON ROAD',
                    AllowContactByEmail: 'Y',
                    AllowContactByPhone: 'Y',
                    AllowContactByPostal: 'Y',
                    BirthDate: '0',
                    BusinessExt: '234',
                    BusinessPhone: '5052342563',
                    BusinessPhoneExtension: '1234',
                    // CCCD: '0', // CCCD Used in example doc but not in request spec
                    CellPhone: '0',
                    City: 'SANTAQUIN',
                    CustomerNumber: '1038979',
                    Email1: 'TESTEMAIL1@TEST.COM',
                    Email2: 'TESTEMAIL2@TEST.COM',
                    ExternalCrossReferenceKey: '0',
                    FaxNumber: '0',
                    FirstName: 'AARON',
                    InternationalBusinessPhone: '5052342563',
                    InternationalHomePhone: '5052568745',
                    // LastChangeDate: '20140602', //  Used in example doc but not in request spec
                    LastName: 'JONES',
                    OtherPhone: '0',
                    PagePhone: '0',
                    PhoneNumber: '5052568745',
                    StateCode: 'UT',
                    TypeCode: 'I',
                    ZipCode: '87048',
                });

                // expect(normalizeFileContents(request)).toBe(normalizeFileContents(expectedXML));
                xmlDiffCheck(request, expectedXML);
            });
        });
    });

    describe('responses', () => {

        describe('getClosedRepairOrders', () => {

            jasmine.DEFAULT_TIMEOUT_INTERVAL = 1000 * 60 * 60 * 1;
            xit('test no results, fetch customers', async () => {
                const zipCodes = [
                    '97739',
                    '97701',
                    '97702',
                    '97708',
                    '97709',
                    '97759',
                    '97712',
                    '97756',
                    '97739',
                    '97759',
                    '97707',
                    '97759',
                ];
                const username = process.env.OPENTRACK_USERNAME;
                const password = process.env.OPENTRACK_PASSWORD;

                const liveEndpoint = 'https://ot.dms.dealertrack.com/customerapi.asmx';
                const companyNumber = process.env.OPENTRACK_COMPANY_NUMBER;
                const companyCode = process.env.OPENTRACK_COMPANY_CODE;
                const serverName = process.env.OPENTRACK_SERVER_NAME;


                const liveApi = (await OpenTrackCustomerAPI.create(liveEndpoint))
                    .dealer(companyCode, companyNumber, serverName)
                    .authenticatedAs(username, password);

                for (const zipCode of zipCodes) {
                    const testParams = { ZipCode: zipCode };
                    const filename = `customer-list.zip-${zipCode}.xml`;
                    console.log({ testParams, filename });
                    const results = await testRequest(liveApi, 'customerList', testParams, getFilePath(filename));
                    // expect(results.Result.length).not.toEqual(0);
                    console.dir({ results }, { depth: null });
                }

                // console.dir({results}, {depth: null});

                // expect(typeof results).toEqual('object');
                // expect(typeof results.Result).toEqual('object');
                // expect(Array.isArray(results.Result)).toBe(true, 'Result was not an array');
                // expect(Array.isArray(results.Errors)).toBe(true, 'Result was not an array');
            });
        });
    });

    function searchBody(method: string, params?: { [key: string]: string | object }) {

        return [jasmine.objectContaining({
            Body: {
                [method]: jasmine.objectContaining({
                    ...params,
                }),
            },
        }), method];
    }

    function mockAuthenticatedApi(baseApi: OpenTrackCustomerAPI) {
        return baseApi.authenticatedAs(mockAuth.username, mockAuth.password)
            .dealer(mockDealer.enterpriseCode, mockDealer.companyNumber, mockDealer.serverName);
    }

    function xmlDiffCheck(test: string, expected: string, message?: string) {
        // expect(normalizeFileContents(test)).toBe(normalizeFileContents(expected));
        return expect(convert.xml2js(test)).toEqual(convert.xml2js(expected), message);
    }

    async function mockSingleResponseAPI(filename: string, mockEndpoint = endpoint) {
        const responseXML = getResponseXML(filename);
        const singleServerConfig = createSingleResponseServer(responseXML, port);
        singleServerConfig.server.on('connection', (socket) => {
            socket.on('close', () => {
                singleServerConfig.server.close();
            });
        });
        const mockAPI = (await OpenTrackCustomerAPI.create(mockEndpoint));
        return mockAuthenticatedAPI(mockAPI);
    }

    function getResponseXML(filename: string) {
        return readFileSync(getFilePath(filename), 'utf8');
    }

    function getFilePath(filename: string) {
        return `${__dirname}/spec/example-responses/customer/${filename}`;
    }
});
