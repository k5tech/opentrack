import { APIAddResult, APIListResult, APIUpdateResult, Omit, OpenTrackAPIBase } from './opentrack.api';
import { DataToken, PhoneNumber } from './opentrack.api.types';

type AlphaBoolean = 'Y' | 'N';
type CustomerType = 'R' | 'W';

export interface Customer {
    CompanyNumber: string;
    CustomerNumber: string;
    EntityType: 'C' | 'I';
    LastName: string;
    FirstName: string;
    MiddleName: string;
    Salutation: string;
    Address1: string;
    Address2: string;
    Address3: string;
    City: string;
    State: string;
    ZipCode: string;
    Country: string;
    HomePhone: PhoneNumber;
    CellPhone: PhoneNumber;
    BusinessPhone: PhoneNumber;
    BusinessPhoneExt: string;
    FaxPhone: PhoneNumber;
    Email1: string;
    Email2: string;
    Vehicles: {
        VIN: string;
    };
    CCCD: number;
}

export interface CustomerAddRequest {
    TypeCode: string;
    LastName: string;
    FirstName?: string;
    MiddleName?: string;
    Salutation?: string;
    Gender?: string;
    Language?: string;
    Address1?: string;
    Address2?: string;
    Address3?: string;
    City?: string;
    Country?: string;
    StateCode?: string;
    ZipCode?: string;
    PhoneNumber?: PhoneNumber;
    BusinessPhone?: PhoneNumber;
    BusinessExt?: string;
    FaxNumber?: PhoneNumber;
    BirthDate?: string;
    DriversLicense?: string;
    Contact?: string;
    PreferredContact?: string;
    MailCode?: string;
    TaxExmptNumber?: string;
    AssignedSalesperson?: string;
    CustomerType?: CustomerType;
    PreferredPhone?: 'B' | 'H' | 'C' | 'P';
    CellPhone?: PhoneNumber;
    PagePhone?: PhoneNumber;
    OtherPhone?: PhoneNumber;
    OtherPhoneDesc?: string;
    Email1?: string;
    Email2?: string;
    OptionalField?: string;
    AllowContactByPostal?: AlphaBoolean;
    AllowContactByPhone?: AlphaBoolean;
    AllowContactByEmail?: AlphaBoolean;
    BusinessPhoneExtension?: string;
    InternationalBusinessPhone?: PhoneNumber;
    InternationalCellPhone?: PhoneNumber;
    ExternalCrossReferenceKey?: string;
    InternationalFaxNumber?: PhoneNumber;
    InternationalOtherNumber?: PhoneNumber;
    InternationalHomePhone?: PhoneNumber;
    CustomerPreferredName?: string;
    PreferredLanguage?: string;
    InternationalZipCode?: string;
    DataToken: string;
}

export type CustomerAddParams = Omit<CustomerAddRequest, 'DataToken'>;
export interface CustomerUpdateParams extends CustomerAddParams {
    CustomerNumber: string;
}
export type CustomerSearchParms = Partial<{
    AllowContactByEmail: string;
    AllowContactByPhone: string;
    AllowContactByPostal: string;
    BirthDate: Date;
    CreatedDateTimeEnd: Date;
    CreatedDateTimeStart: Date;
    Email: string;
    FirstName: string;
    LastName: string;
    Phone: PhoneNumber;
    State: string;
    UpdatedDateTimeEnd: Date;
    UpdatedDateTimeStart: Date;
    ZipCode: string;
}>;

export type CustomerLookupParms = Partial<{
    CustomerNumber: string;
    CommonCustomerID: string;
    CommonCustomerVersion: string;
}>;

export type CustomerListParms = Partial<{
    ZipCode: string;
    IncludeCompanies: string;
    ExcludeBlankAddress: string;
    ExcludeBlankPhone: string;
    ExcludeBlankEmail: string;
}>;

export class OpenTrackCustomerAPI extends OpenTrackAPIBase {
    public port = 'CustomerApiSoap';

    public static async create(endpoint = 'https://otstaging.arkona.com/opentrack/customerapi.asmx') {
        const a = new this(endpoint);
        await a.setupSoapClient();
        return a;
    }

    constructor(endpoint = 'https://otstaging.arkona.com/opentrack/customerapi.asmx') {
        // super('/opentrack/customerapi.asmx', username, password);
        super(endpoint);
    }

    public async customerSearch(searchParams: CustomerSearchParms) {
        // Maybe update phone if E.164
        if (typeof searchParams.Phone === 'string'
            && searchParams.Phone.length
            && searchParams.Phone.length > 10
        ) {
            searchParams.Phone = searchParams.Phone.substring(searchParams.Phone.length - 10);
        }
        const args = { ...this.defaultParams(), SearchParms: searchParams };
        return this.runApiMethod<APIListResult<Customer>>('CustomerSearch', args);
    }

    public async customerLookup(lookupParams: CustomerLookupParms) {
        // const args = { ...this.defaultParams(), LookupParms: lookupParams };
        const args = { ...this.defaultParams(), ...lookupParams };
        return this.runApiMethod<APIListResult<Customer>>('CustomerLookup', args);
    }

    public async customerAdd(dataToken: DataToken, customer: CustomerAddParams) {
        const args = { ...this.defaultParams(), Customer: {...customer, DataToken: dataToken} };
        return this.runApiMethod<APIAddResult>('CustomerAdd', args);
    }

    public async customerUpdate(customer: CustomerUpdateParams) {
        const args = { ...this.defaultParams(), Customer: {...customer} };
        return this.runApiMethod<APIUpdateResult>('CustomerUpdate', args);
    }

    public async customerList(listParms: CustomerListParms) {
        const args = { ...this.defaultParams(), ListParms: listParms };
        return this.runApiMethod<APIListResult<Customer>>('CustomerList', args);
    }

    public async getApi() {
        return (await this.soapClient()).CustomerApi;
    }

    public async describeDetail() {
        return (await this.describe()).CustomerApi[this.port];
    }

}
