import { readFileSync } from 'fs';
import { Server } from 'http';
import { Server as SoapServer } from 'soap';
import * as convert from 'xml-js';
import { OpenTrackAPIBase } from './opentrack.api';
import { isClosedRepairOrder, OpenTrackServiceAPI } from './opentrack.service.api';
import {
    createMockServer, createSingleResponseServer, mockAuthenticatedAPI,
    mockRequestDate, testRequest,
} from './spec/mock-server';

describe('OpenTrack Service API', () => {
    const port = 8001;
    const endpoint = `http://localhost:${port}`;

    const testLookupConfig = {
        // orderTargetDate: new Date('2017-11-27T22:02:51.000Z'),
        customerKey: '1039974', // Not used
        customerName: 'SOLO, HAN',
        customerNumber: '1039974',
        customerPhoneNumber: '5551239874', // Change
        internalKey: '15030',
        orderTargetDate: new Date('2017-11-08T22:02:51.000Z'),
        repairOrderNumber: '6043478',
        vin: '131388131388',
    };

    describe('requests', () => {
        let api: OpenTrackServiceAPI;
        let server: Server;
        let soapServer: SoapServer;

        beforeEach(async () => {
            ({ api, server, soapServer } = await setupMockServices(endpoint, port));
        });
        afterEach((done) => {
            jasmine.clock().uninstall();
            server.close(() => {
                server = null;
                soapServer = null;
                done();
            });
        });

        describe('getClosedRepairOrders', () => {
            const expectedMethod = 'GetClosedRepairOrders';

            it('should lookup by customer number', async () => {
                const requestSpy = jasmine.createSpy('request');
                soapServer.on('request', requestSpy);

                await api.getClosedRepairOrders({ CustomerNumber: testLookupConfig.customerNumber });

                expect(requestSpy).toHaveBeenCalledWith(...searchBody(expectedMethod, {
                    request: {
                        CustomerNumber: 1039974,
                    },
                }));
            });

            it('should request with expected xml', async () => {
                let request: string;
                const authenticatedApi = mockAuthenticatedAPI(api);
                const filename = 'get-closed-repair-orders.request-1-formatted.xml';
                const expectedXML = readFileSync(`${__dirname}/spec/example-requests/service/${filename}`, 'utf8');

                soapServer.log = (type: string, data) => {
                    if (type === 'received') {
                        request = data;
                    }
                };
                jasmine.clock().mockDate(mockRequestDate);

                await authenticatedApi.getClosedRepairOrders({ CustomerNumber: testLookupConfig.customerNumber });

                xmlDiffCheck(request, expectedXML);
            });

            it('should request with expected xml example 2', async () => {
                let request: string;
                const authenticatedApi = mockAuthenticatedAPI(api);
                const filename = 'get-closed-repair-orders.request-2-formatted.xml';
                const expectedXML = readFileSync(`${__dirname}/spec/example-requests/service/${filename}`, 'utf8');

                soapServer.log = (type: string, data) => {
                    if (type === 'received') {
                        request = data;
                    }
                };
                jasmine.clock().mockDate(mockRequestDate);

                await authenticatedApi.getClosedRepairOrders({
                    CloseDate: '2015-11-24',
                    CustomerNumber: '1039974',
                    RepairOrderNumber: '9001414',
                    VIN: '131388131388',
                });

                xmlDiffCheck(request, expectedXML);
            });
        });

        describe('openRepairOrderLookup', () => {

            it('should fetch all', async () => {
                const requestSpy = jasmine.createSpy('request').and.callFake((test) => {
                    // console.log(test);
                });
                soapServer.on('request', requestSpy);

                const start = new Date(testLookupConfig.orderTargetDate);
                const end = new Date(testLookupConfig.orderTargetDate);
                start.setDate(start.getDate() - 1);
                end.setDate(end.getDate() + 1);

                const apiResponse = await api.openRepairOrderLookup({
                    CreatedDateTimeEnd: end.toISOString(),
                    CreatedDateTimeStart: start.toISOString(),
                    InternalOnly: 'Y',
                });

                expect(requestSpy).toHaveBeenCalledWith(...searchBody('OpenRepairOrderLookup', {
                    LookupParms: {
                        CreatedDateTimeEnd: new Date('2017-11-09T22:02:51Z'),
                        CreatedDateTimeStart: new Date('2017-11-07T22:02:51Z'),
                        InternalOnly: 'Y',
                    },
                }));
            });

            it('should lookup by vin', async () => {
                const requestSpy = jasmine.createSpy('request');
                soapServer.on('request', requestSpy);

                const apiResponse = await api.openRepairOrderLookup({ VIN: testLookupConfig.vin });

                expect(requestSpy).toHaveBeenCalledWith(...searchBody('OpenRepairOrderLookup', {
                    LookupParms: {
                        VIN: '131388131388',
                    },
                }));
            });

            it('should lookup by customer name', async () => {
                const requestSpy = jasmine.createSpy('request');
                soapServer.on('request', requestSpy);

                await api.openRepairOrderLookup({ CustomerName: testLookupConfig.customerName });

                expect(requestSpy).toHaveBeenCalledWith(...searchBody('OpenRepairOrderLookup', {
                    LookupParms: {
                        CustomerName: 'SOLO, HAN',
                    },
                }));
            });

            it('should lookup by customer number', async () => {
                const requestSpy = jasmine.createSpy('request');
                soapServer.on('request', requestSpy);
                await api.openRepairOrderLookup({ CustomerNumber: testLookupConfig.customerNumber });
                expect(requestSpy).toHaveBeenCalledWith(...searchBody('OpenRepairOrderLookup', {
                    LookupParms: {
                        CustomerNumber: '1039974', // It's a string here...
                    },
                }));
            });

            it('should lookup by internal key', async () => {
                const requestSpy = jasmine.createSpy('request');
                soapServer.on('request', requestSpy);

                const apiResponse = await api.openRepairOrderLookup({ InternalKey: testLookupConfig.internalKey });

                expect(requestSpy).toHaveBeenCalledWith(...searchBody('OpenRepairOrderLookup', {
                    LookupParms: {
                        InternalKey: '15030',
                    },
                }));
                // const firstRecord = apiResponse.Result[0];
                // expect(apiResponse.Result.length).toBe(1);
                // expect(firstRecord.CustomerName).toBe(testLookupConfig.customerName);
            });

            it('should lookup by repair order id', async () => {
                const requestSpy = jasmine.createSpy('request');
                soapServer.on('request', requestSpy);

                const apiResponse = await api.openRepairOrderLookup({
                    RepairOrderNumber: testLookupConfig.repairOrderNumber,
                });

                expect(requestSpy).toHaveBeenCalledWith(...searchBody('OpenRepairOrderLookup', {
                    LookupParms: {
                        RepairOrderNumber: '6043478',
                    },
                }));

                // const firstRecord = apiResponse.Result[0];
                // expect(apiResponse.Result.length).toBe(1);
                // expect(firstRecord.CustomerName).toBe(testLookupConfig.customerName);
            });

            it('should request with expected xml (VIN Lookup)', async () => {
                let request: string;
                const authenticatedApi = mockAuthenticatedAPI(api);
                const filename = 'open-repair-order-lookup.request-1-formatted.xml';
                const expectedXML = readFileSync(`${__dirname}/spec/example-requests/service/${filename}`, 'utf8');

                soapServer.log = (type: string, data) => {
                    if (type === 'received') {
                        request = data;
                    }
                };
                jasmine.clock().mockDate(mockRequestDate);

                await authenticatedApi.openRepairOrderLookup({ VIN: '131388131388' });

                xmlDiffCheck(request, expectedXML);
            });

            it('should request with expected xml example 2', async () => {
                let request: string;
                const authenticatedApi = mockAuthenticatedAPI(api);
                const filename = 'open-repair-order-lookup.request-2-formatted.xml';
                const expectedXML = readFileSync(`${__dirname}/spec/example-requests/service/${filename}`, 'utf8');

                soapServer.log = (type: string, data) => {
                    if (type === 'received') {
                        request = data;
                    }
                };
                jasmine.clock().mockDate(mockRequestDate);

                await authenticatedApi.openRepairOrderLookup({
                    CreatedDateTimeEnd: '2016-12-31',
                    CreatedDateTimeStart: '2016-12-01',
                });

                xmlDiffCheck(request, expectedXML);
            });

        });
    });

    describe('responses', () => {

        describe('getClosedRepairOrders', () => {

            it('handles no results', async () => {
                const filename = 'get-closed-repair-orders.no-records.response-1-formatted.xml';
                const testParams = { CustomerNumber: testLookupConfig.customerNumber };
                const results = await (await mockSingleResponseAPI(filename)).getClosedRepairOrders(testParams);

                expect(typeof results).toEqual('object');
                expect(typeof results.Result).toEqual('object');
                expect(Array.isArray(results.Result)).toBe(true, 'Result was not an array');
                expect(results.Result.length).toEqual(0);
                // expect(Array.isArray(results.Errors)).toBe(true, 'Result was not an array');
            });

            it('handles multiple results', async () => {
                const filename = 'get-closed-repair-orders.response-1-formatted.xml';
                const testParams = { CustomerNumber: testLookupConfig.customerNumber };
                const results = await (await mockSingleResponseAPI(filename)).getClosedRepairOrders(testParams);

                expect(typeof results).toEqual('object');
                expect(Array.isArray(results.Result)).toBe(true, 'Result was not an array');
                expect(results.Result.length).toEqual(5);
                for (const result of results.Result) {
                    expect(isClosedRepairOrder(result)).toBe(true, `record failed type guard`);
                }
            });

            it('handles single results', async () => {
                const filename = 'get-closed-repair-orders.response-2-formatted.xml';
                const testParams = { RepairOrderNumber: '9001414' };
                const results = await (await mockSingleResponseAPI(filename)).getClosedRepairOrders(testParams);

                expect(typeof results).toEqual('object');
                expect(Array.isArray(results.Result)).toBe(true, 'Result was not an array');
                expect(results.Result.length).toEqual(1);
                for (const result of results.Result) {
                    expect(isClosedRepairOrder(result)).toBe(true, `record failed type guard`);
                }
            });

        });

        describe('openRepairOrderLookup', () => {

            it('handles no results', async () => {
                const filename = 'open-repair-order-lookup.no-records.response-1-formatted.xml';
                const testParams = { RepairOrderNumber: '123' };
                const results = await (await mockSingleResponseAPI(filename)).openRepairOrderLookup(testParams);

                expect(typeof results).toEqual('object');
                expect(typeof results.Errors).toEqual('object');
                expect(typeof results.Errors.Error).toEqual('object');
                // expect(typeof results.Result).toEqual('object');
                expect(Array.isArray(results.Errors.Error)).toBe(true, 'Errors.Error was not an array');
                // expect(results.Result.length).toEqual(0);
            });

            it('handles single results', async () => {
                const filename = 'open-repair-order-lookup.response-1-formatted.xml';
                const testParams = { RepairOrderNumber: '6000037' };
                const results = await (await mockSingleResponseAPI(filename)).openRepairOrderLookup(testParams);
                // const results = await testRequest(
                //     await OpenTrackServiceAPI.create(), 'openRepairOrderLookup', testParams, getFilePath(filename),
                // );

                expect(typeof results).toEqual('object');
                expect(Array.isArray(results.Result)).toBe(true, 'Result was not an array');
                expect(results.Result.length).toEqual(1);
                for (const result of results.Result) {
                    expect(isClosedRepairOrder(result)).toBe(true, `record failed type guard`);
                }
            });

        });
    });

    async function mockSingleResponseAPI(filename: string, mockEndpoint = endpoint) {
        const responseXML = getResponseXML(filename);
        const singleServerConfig = createSingleResponseServer(responseXML, port);
        singleServerConfig.server.on('connection', (socket) => {
            socket.on('close', () => {
                singleServerConfig.server.close();
            });
        });
        const mockAPI = (await OpenTrackServiceAPI.create(mockEndpoint));
        return mockAuthenticatedAPI(mockAPI);
    }

    function searchBody(method: string, params?: { [key: string]: string | object }) {

        return [jasmine.objectContaining({
            Body: {
                [method]: jasmine.objectContaining({
                    ...params,
                }),
            },
        }), method];
    }

    function xmlDiffCheck(test: string, expected: string, message?: string) {
        return expect(convert.xml2js(test)).toEqual(convert.xml2js(expected), message);
    }

    function getResponseXML(filename: string) {
        return readFileSync(getFilePath(filename), 'utf8');
    }

    function getFilePath(filename: string) {
        return `${__dirname}/spec/example-responses/service/${filename}`;
    }

    async function setupMockServices(mockEndpoint: string, mockPort: number) {
        const xml = readFileSync(`${__dirname}/spec/service.wsdl`, 'utf8')
            .replace('<soap:address location="https://otstaging.arkona.com/opentrack/serviceapi.asmx" />',
                `<soap:address location="${mockEndpoint}" />`);
        const serverConfig = createMockServer(xml, mockPort);
        const mockServer = serverConfig.server;
        const mockSoapServer = serverConfig.soapServer;
        const mockAPI = (await OpenTrackServiceAPI.create(mockEndpoint));
        return {
            api: mockAPI,
            server: mockServer,
            soapServer: mockSoapServer,
        };
    }
});
