import { readFileSync, writeFileSync } from 'fs';
import { Server } from 'http';
import { Server as SoapServer } from 'soap';
import * as convert from 'xml-js';
import { OpenTrackPartsAPI } from './opentrack.parts.api';
import { createMockServer, createSingleResponseServer, mockAuth, mockDealer, mockRequestDate } from './spec/mock-server';

describe('OpenTrack Parts API', () => {
    const port = 8002;
    let api: OpenTrackPartsAPI;
    let server: Server;
    let soapServer: SoapServer;

    describe('requests', () => {

        beforeEach(async () => {
            const location = `http://localhost:${port}`;

            const xml = readFileSync(`${__dirname}/spec/parts.wsdl`, 'utf8')
                .replace(
                    '<soap:address location="https://otstaging.arkona.com/opentrack/partsapi.asmx" />',
                    `<soap:address location="${location}" />`);
            const serverConfig = createMockServer(xml, port);
            server = serverConfig.server;
            soapServer = serverConfig.soapServer;
            api = (await OpenTrackPartsAPI.create(location));
        });
        afterEach((done) => {
            jasmine.clock().uninstall();
            if (server !== null) {
                server.close(() => {
                    server = null;
                    soapServer = null;
                    done();
                });
            } else {
                done();
            }
        });

        describe('salesHistory', () => {
            it('should request with expected xml', async () => {
                let request: string;
                const authenticatedApi = mockAuthenticatedApi(api);
                const filename = 'parts-sales-history.request-1-formatted.xml';
                const expectedXML = getRequestXML(filename);

                soapServer.log = (type: string, data) => {
                    if (type === 'received') {
                        request = data;
                    }
                };
                jasmine.clock().mockDate(mockRequestDate);

                await authenticatedApi.salesHistory({
                    DateStart: '20100401',
                    DateEnd: '20100501',
                });

                xmlDiffCheck(request, expectedXML);
            });
        });

    });
    describe('responses', () => {
        afterEach((done) => {
            if (server !== null) {
                server.close(() => {
                    server = null;
                    soapServer = null;
                    done();
                });
            } else {
                done();
            }
        });
        describe('salesHistory', () => {

            it('handles no results', async () => {
                const filename = 'parts-sales-history.no-records.response-1-formatted.xml';
                const authenticatedApi = mockAuthenticatedApi(api);
                const responseXML = getResponseXML(filename);
                const singleServerConfig = createSingleResponseServer(responseXML, port);
                server = singleServerConfig.server;

                const results = await authenticatedApi.salesHistory({
                    DateStart: '20180120',
                    DateEnd: '20180201',
                });

                expect(typeof results).toEqual('object');
                expect(typeof results.Errors).toEqual('object');
                expect(results.Errors.Error[0].Code).toEqual('PSH01');
                expect(results.Errors.Error[0].Message).toEqual('No records found.');
                // expect(Array.isArray(results.Errors)).toBe(true, 'Result was not an array');
            });

            it('handles multiple results', async () => {
                const filename = 'parts-sales-history.response-1-formatted.xml';
                const authenticatedApi = mockAuthenticatedApi(api);
                const responseXML = getResponseXML(filename);
                const singleServerConfig = createSingleResponseServer(responseXML, port);
                server = singleServerConfig.server;

                const results = await authenticatedApi.salesHistory({
                    DateStart: '20180120',
                    DateEnd: '20180201',
                });

                // console.dir({ results }, { depth: null });
                expect(typeof results).toEqual('object'); ;
                expect(Array.isArray(results.Result)).toBe(true, 'Result was not an array');
                expect(results.Result.length).toEqual(2);
            });

            it('handles single results', async () => {
                const filename = 'parts-sales-history.response-2-formatted.xml';
                const authenticatedApi = mockAuthenticatedApi(api);
                const responseXML = getResponseXML(filename);
                const singleServerConfig = createSingleResponseServer(responseXML, port);
                server = singleServerConfig.server;

                const results = await authenticatedApi.salesHistory({
                    DateStart: '20180620',
                    DateEnd:   '20180625',
                });

                // console.dir({ results }, { depth: null });
                expect(typeof results).toEqual('object');
                expect(Array.isArray(results.Result)).toBe(true, 'Result was not an array');
                expect(results.Result.length).toEqual(1);
            });
        });
    });

    function getRequestXML(filename: string) {
        return readFileSync(`${__dirname}/spec/example-requests/parts/${filename}`, 'utf8');
    }

    function getResponseXML(filename: string) {
        return readFileSync(`${__dirname}/spec/example-responses/parts/${filename}`, 'utf8');
    }

    function writeResponseXML(filename: string, content: any) {
        return writeFileSync(`${__dirname}/spec/example-responses/parts/${filename}`, content, 'utf8');
    }

    function mockAuthenticatedApi(baseApi: OpenTrackPartsAPI) {
        return baseApi.authenticatedAs(mockAuth.username, mockAuth.password)
            .dealer(mockDealer.enterpriseCode, mockDealer.companyNumber, mockDealer.serverName);
    }

    function xmlDiffCheck(test: string, expected: string, message?: string) {
        return expect(convert.xml2js(test)).toEqual(convert.xml2js(expected), message);
    }
});
