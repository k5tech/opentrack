import { renameKeys } from './helpers';
import { APIListResult, OpenTrackAPIBase } from './opentrack.api';

export function isClosedRepairOrder(thing: any): thing is ClosedRepairOrder {
    if (typeof thing === 'object'
    && 'CompanyNumber' in thing
    && 'RepairOrderNumber' in thing
) {
    return true;
} else {
    return false;
}
}
export interface ClosedRepairOrder {
    CompanyNumber: string;
    RepairOrderNumber: string;
}

export type GetClosedRepairOrdersRequest = Partial<{
    VIN: string;
    RepairOrderNumber: string;
    CustomerNumber: string;
    CloseDate: string;
    FinalCloseDate: string;
    // CreatedDateTimeStart: Date;
    CreatedDateTimeStart: string;
    CreatedDateTimeEnd: string;
    FinalCloseDateEnd: string;
    FinalCloseDateStart: string;
}>;

export interface OpenRepairOrderLookupParms {
    CustomerName: string;
    CustomerNumber: string;
    CreatedDateTimeEnd: string;
    CreatedDateTimeStart: string;
    InternalKey: string;
    InternalOnly: 'Y';
    RepairOrderNumber: string;
    StockNumber: string;
    TagNumber: string;
    VIN: string;
    VIN6: string;
    ModifiedAfter: string;
}

export interface RepairOrderLookupResult {
    CompanyNumber: string;
    OpenTransactionDate: string;
    InternalKey: string;
    RepairOrderNumber: string;
    CustomerNumber: string;
    CustomerName: string;
    PartsTotal: string;
    ShippingTotal: string;
    TotalEstimate: string;

}
export class OpenTrackServiceAPI extends OpenTrackAPIBase {

    public port = 'ServiceApiSoap';

    public static async create(endpoint = 'https://otstaging.arkona.com/opentrack/serviceapi.asmx') {
        const a = new this(endpoint);
        await a.setupSoapClient();
        return a;
    }
    constructor(endpoint = 'https://otstaging.arkona.com/opentrack/serviceapi.asmx') {
        super(endpoint);
    }

    public async getClosedRepairOrders(request: GetClosedRepairOrdersRequest) {
        const args = renameKeys({ Dealer: 'dealer' }, { ...this.defaultParams(), request });
        return this.runApiMethod<{
            GetClosedRepairOrdersResult?: { ClosedRepairOrders: {ClosedRepairOrder: ClosedRepairOrder[]} };
        }>('GetClosedRepairOrders', args).then((results) => {
            if (!results.GetClosedRepairOrdersResult) {
                return {Result: []};
            }
            if (!results.GetClosedRepairOrdersResult.ClosedRepairOrders) {
                return {Result: []};
            }
            return { Result: results.GetClosedRepairOrdersResult.ClosedRepairOrders.ClosedRepairOrder || []};
        });
    }

    public async openRepairOrderLookup(lookupParams: Partial<OpenRepairOrderLookupParms>) {
        const args = { ...this.defaultParams(), LookupParms: lookupParams };
        return this.runApiMethod<APIListResult<RepairOrderLookupResult>>('OpenRepairOrderLookup', args);
    }

    public async getApi() {
        return (await this.soapClient()).ServiceApi;
    }

}
