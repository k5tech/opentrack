import { OpenTrackAPIBase } from './opentrack.api';

export interface PartsSalesDetail {
    CompanyNumber: string;
}
export interface PartsSalesComment {
    Comments: string;
}
export interface PartsSalesHistoryResult {
    Result?: Array<{
        Details: PartsSalesDetail[];
        Comments: PartsSalesComment[];
    }>;
    Errors?: {
        Error: Array<{
            Code: string;
            Message: string;
        }>;
    };
}

// TODO: Define which options are optional
interface SalesHistoryParms {
    InvoiceNumber?: string;
    DaysBack?: string;
    DateStart?: string;
    DateEnd?: string;
    SaleType?: string;
    Manufacture?: string;
    CustomerKey?: string;
    PurchaseOrderNumber?: string;
}

export class OpenTrackPartsAPI extends OpenTrackAPIBase {

    public port = 'PartsApiSoap';
    public static async create(endpoint = 'https://otstaging.arkona.com/opentrack/partsapi.asmx') {
        const a = new this(endpoint);
        await a.setupSoapClient();
        return a;
    }
    constructor(endpoint = 'https://otstaging.arkona.com/opentrack/partsapi.asmx') {
        super(endpoint);
    }

    public async salesHistory(salesHistoryParams: SalesHistoryParms) {
        const args = { ...this.defaultParams(), SalesHistoryParms: salesHistoryParams };
        return this.runApiMethod<PartsSalesHistoryResult>('PartsSalesHistory', args);
    }

    public async getApi() {
        return (await this.soapClient()).PartsApi;
    }
}
