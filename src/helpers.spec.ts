import { renameKeys, renameProp } from './helpers';

describe('Helper functions', () => {
    describe('renameProp', () => {
        it('renames single properties', () => {
            expect(renameProp('key', 'newKey', { key: 'value' })).toEqual({ newKey: 'value' });
            expect(renameProp('key', 'someOtherKey', { key: 'value' })).toEqual({ someOtherKey: 'value' });
        });

        it('renames single on multi property objects', () => {
            const base = {
                a: 'ValueA',
                b: 'ValueB',
                c: 'ValueC',
                d: 'ValueD',
            };
            expect(renameProp('b', 'X', base)).toEqual({
                a: 'ValueA',
                X: 'ValueB',
                c: 'ValueC',
                d: 'ValueD',
            });
            expect(renameProp('d', 'Z', base)).toEqual({
                a: 'ValueA',
                b: 'ValueB',
                c: 'ValueC',
                Z: 'ValueD',
            });
        });
    });

    describe('renameKeys', () => {
        it('renames single properties', () => {
            expect(renameKeys({ key: 'newKey' }, { key: 'value' })).toEqual({ newKey: 'value' });
            expect(renameKeys({ key: 'someOtherKey' }, { key: 'value' })).toEqual({ someOtherKey: 'value' });
        });

        it('renames multiple properties', () => {
            const base = {
                a: 'ValueA',
                b: 'ValueB',
                c: 'ValueC',
                d: 'ValueD',
            };
            expect(renameKeys({ b: 'X' }, base)).toEqual({
                a: 'ValueA',
                X: 'ValueB',
                c: 'ValueC',
                d: 'ValueD',
            });
            expect(renameKeys({ b: 'X', d: 'Z' }, base)).toEqual({
                a: 'ValueA',
                X: 'ValueB',
                c: 'ValueC',
                Z: 'ValueD',
            });
            // expect(renameKeys({key: 'someOtherKey'}, {key: 'value'})).toEqual({someOtherKey: 'value'});
        });
    });

});
