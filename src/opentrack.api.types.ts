import { isString } from 'util';

export type DataToken = string;

export function isDataToken(value: any): value is DataToken {
    if (isString(value) && value.length < 25) {
        return true;
    }
    return false;
}

export type PhoneNumber = string;
export function isPhoneNumber(value: any): value is PhoneNumber {
    if (isString(value) && value.length <= 10) {
        return true;
    }
    return false;
}
