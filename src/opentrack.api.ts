import * as soap from 'soap';
import { DataToken } from './opentrack.api.types';

export interface OpenTrackAPI {
    endpoint: string;
}
export interface APIListResult<T> {
    Token: DataToken;
    Errors?: {
        Error: APIError[];
    };
    Result?: T[];
}
export interface APIError {
    Code: string;
    Message: string;
}

export interface APIAddResult {
    Message: string;
    Code: string;
}

export interface APIUpdateResult {
    Errors?: {
        Error: APIError[];
    };
    Message: string;
    Key: string;
    Token: string;
}

// Remove properties from a type
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;

export abstract class OpenTrackAPIBase implements OpenTrackAPI {

    private lastRawResponseXML: string | null = null;

    get wsdl() {
        return `${this.endpoint}?WSDL`;
    }

    get lastResponseXML() {
        return this.lastRawResponseXML;
    }

    private soapInstance: soap.Client;
    private requestParams: { [key: string]: string | object } = {
        Dealer: null,
    };

    public port: string;

    constructor(public endpoint: string = '') { }

    // abstract async create(): this;
    // public static async create(endpoint: string = ''): Promise<any> {
    //     throw new Error('create must be extended by child class');
    // }

    // tslint:disable-next-line:ban-types
    public abstract async getApi(): Promise<soap.ISoapMethod | soap.WSDL | Function>;

    public dealer(enterpriseCode: string, companyNumber: string, serverName: string) {
        this.requestParams.Dealer = {
            CompanyNumber: companyNumber,
            EnterpriseCode: enterpriseCode,
            ServerName: serverName,
        };
        return this;
    }

    public defaultParams() {
        return this.requestParams;
    }

    public async runApiMethod<T>(method: string, args?: any) {
        this.soapInstance.on('response', (body, response, eid) => this.lastRawResponseXML = body);
        const api = await this.getApi();
        return new Promise<T>((resolve, reject) => {
            if (typeof api[this.port][method] === 'function') {
                api[this.port][method](args, (err: any, result: any) => {
                    if (err) {
                        reject(this.getErrorMessage(err));
                        throw new Error(this.getErrorMessage(err));
                    } else {
                        resolve(result);
                    }
                });
            } else {
                reject(new Error(`${this.port} - ${method} is not a function`));
            }
        });
    }

    public async setupSoapClient() {
        this.soapInstance = await soap.createClientAsync(this.wsdl);
    }

    public authenticatedAs(username: string, password: string, options?: any) {
        this.soapInstance.setSecurity(new soap.WSSecurity(username, password, options));
        return this;
    }

    public async soapClient() {
        if (typeof this.soapInstance === 'undefined') {
            await this.setupSoapClient();
        }
        return this.soapInstance;
    }

    public async describe() {
        return (await this.soapClient()).describe();
    }

    public getErrorMessage(errObject: any) {
        if (errObject.Errors && errObject.Errors.Error) {
            return errObject.Errors.Error;
        }
        return errObject;
    }

    public lastRequest() {
        return this.soapInstance.lastRequest;
    }
}
