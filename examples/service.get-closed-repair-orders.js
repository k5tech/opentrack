// import { OpenTrackServiceAPI } from '@k5tech/opentrack';
const { OpenTrackServiceAPI } = require('@k5tech/opentrack');

const username = 'username'
const password = 'password'
const enterpriseCode = 'ZE';
const companyNumber = 'ZE7';
const serverName = 'arkonap.arkona.com'

const api = (await OpenTrackServiceAPI.create())
                .authenticatedAs(username, password)
                .dealer(enterpriseCode, companyNumber, serverName);

const apiResponse = await api.getClosedRepairOrders({CustomerNumber: '1039974'});

console.log(apiResponse);