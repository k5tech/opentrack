// import { OpenTrackServiceAPI } from '@k5tech/opentrack';
const { OpenTrackServiceAPI } = require('@k5tech/opentrack');

const username = 'username'
const password = 'password'
const enterpriseCode = 'ZE';
const companyNumber = 'ZE7';
const serverName = 'arkonap.arkona.com'

const now = new Date('2017-11-08T22:02:51.000Z');
const start = new Date(now);
const end = new Date(now);
start.setDate(start.getDate() - 1);
end.setDate(end.getDate() + 1);

const api = (await OpenTrackServiceAPI.create())
                .authenticatedAs(username, password)
                .dealer(enterpriseCode, companyNumber, serverName);

const apiResponse = await api.openRepairOrderLookup({
    CreatedDateTimeEnd: end.toISOString(),
    CreatedDateTimeStart: start.toISOString(),
    InternalOnly: 'Y',
});

console.log(apiResponse);