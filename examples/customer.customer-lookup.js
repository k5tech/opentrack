// import { OpenTrackCustomerAPI } from '@k5tech/opentrack';
const { OpenTrackCustomerAPI } = require('@k5tech/opentrack');

const username = 'username'
const password = 'password'
const enterpriseCode = 'ZE';
const companyNumber = 'ZE7';
const serverName = 'arkonap.arkona.com'

const customerNumber = '1039358';

const api = (await OpenTrackCustomerAPI.create())
                .authenticatedAs(username, password)
                .dealer(enterpriseCode, companyNumber, serverName);

const apiResponse = await api.customerLookup({ CustomerNumber: customerNumber });

console.log(apiResponse);