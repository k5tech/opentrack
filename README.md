# OpenTrack API Wrapper

[![NPM Version][npm-image]][npm-url] [![Build Status][pipelines-image]][pipelines-url] [![Coveralls Status][coveralls-image]][coveralls-url]

## Installation

```sh
npm install --save @k5tech/opentrack
```

## Example

```javascript
// import { OpenTrackCustomerAPI } from '@k5tech/opentrack';
const { OpenTrackCustomerAPI } = require('@k5tech/opentrack');

const username = 'username'
const password = 'password'
const enterpriseCode = 'ZE';
const companyNumber = 'ZE7';
const serverName = 'arkonap.arkona.com'

const customerNumber = '1039358';

const api = (await OpenTrackCustomerAPI.create())
                .authenticatedAs(username, password)
                .dealer(enterpriseCode, companyNumber, serverName);

const apiResponse = await api.customerLookup({ CustomerNumber: customerNumber });

console.log(apiResponse);
```

### API Modules

#### Customer

Customer Search

```javascript
// import { OpenTrackCustomerAPI } from '@k5tech/opentrack';
const { OpenTrackCustomerAPI } = require('@k5tech/opentrack');

const username = 'username'
const password = 'password'
const enterpriseCode = 'ZE';
const companyNumber = 'ZE7';
const serverName = 'arkonap.arkona.com'

const phoneNumber = '5558369361';

const api = (await OpenTrackCustomerAPI.create())
                .authenticatedAs(username, password)
                .dealer(enterpriseCode, companyNumber, serverName);

const apiResponse = await api.customerSearch({Phone: phoneNumber});

console.log(apiResponse);
```

Customer Lookup

```javascript
// import { OpenTrackCustomerAPI } from '@k5tech/opentrack';
const { OpenTrackCustomerAPI } = require('@k5tech/opentrack');

const username = 'username'
const password = 'password'
const enterpriseCode = 'ZE';
const companyNumber = 'ZE7';
const serverName = 'arkonap.arkona.com'

const customerNumber = '1039358';

const api = (await OpenTrackCustomerAPI.create())
                .authenticatedAs(username, password)
                .dealer(enterpriseCode, companyNumber, serverName);

const apiResponse = await api.customerLookup({ CustomerNumber: customerNumber });

console.log(apiResponse);
```

#### Service

Open Repair Order Lookup

```javascript
// import { OpenTrackCustomerAPI } from '@k5tech/opentrack';
const { OpenTrackServiceAPI } = require('@k5tech/opentrack');

const username = 'username'
const password = 'password'
const enterpriseCode = 'ZE';
const companyNumber = 'ZE7';
const serverName = 'arkonap.arkona.com'

const now = new Date('2017-11-08T22:02:51.000Z');
const start = new Date(now);
const end = new Date(now);
start.setDate(start.getDate() - 1);
end.setDate(end.getDate() + 1);

const api = (await OpenTrackServiceAPI.create())
                .authenticatedAs(username, password)
                .dealer(enterpriseCode, companyNumber, serverName);

const apiResponse = await api.openRepairOrderLookup({
    CreatedDateTimeEnd: end.toISOString(),
    CreatedDateTimeStart: start.toISOString(),
    InternalOnly: 'Y',
});

console.log(apiResponse);
```

Get Closed Repair Orders

```javascript
// import { OpenTrackServiceAPI } from '@k5tech/opentrack';
const { OpenTrackServiceAPI } = require('@k5tech/opentrack');

const username = 'username'
const password = 'password'
const enterpriseCode = 'ZE';
const companyNumber = 'ZE7';
const serverName = 'arkonap.arkona.com'

const api = (await OpenTrackServiceAPI.create())
                .authenticatedAs(username, password)
                .dealer(enterpriseCode, companyNumber, serverName);

const apiResponse = await api.getClosedRepairOrders({CustomerNumber: '1039974'});

console.log(apiResponse);
```

[OpenTack Wiki](https://www.dealertrackdms.com/opentrack/index.php/Main_Page)

[npm-image]: https://img.shields.io/npm/v/@k5tech/opentrack.svg
[npm-url]: https://npmjs.org/package/@k5tech/opentrack
[pipelines-image]:https://img.shields.io/bitbucket/pipelines/k5tech/opentrack.svg
[pipelines-url]:https://bitbucket.org/k5tech/opentrack/addon/pipelines/home
[coveralls-image]:https://img.shields.io/coveralls/bitbucket/k5tech/opentrack/master.svg
[coveralls-url]:https://coveralls.io/bitbucket/k5tech/opentrack