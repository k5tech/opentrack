[@k5tech/opentrack](../README.md) > ["opentrack.api"](../modules/_opentrack_api_.md)

# External module: "opentrack.api"

## Index

### Classes

* [OpenTrackAPIBase](../classes/_opentrack_api_.opentrackapibase.md)

### Interfaces

* [APIAddResult](../interfaces/_opentrack_api_.apiaddresult.md)
* [APIError](../interfaces/_opentrack_api_.apierror.md)
* [APIListResult](../interfaces/_opentrack_api_.apilistresult.md)
* [OpenTrackAPI](../interfaces/_opentrack_api_.opentrackapi.md)

### Type aliases

* [Omit](_opentrack_api_.md#markdown-header-Omit)

---

## Type aliases

###  Omit

**Ƭ Omit**: *`Pick`<`T`, `Exclude`<`keyof T`, `K`>>*

*Defined in [opentrack.api.ts:25](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-25)*

___

