[@k5tech/opentrack](../README.md) > ["opentrack.api.types"](../modules/_opentrack_api_types_.md)

# External module: "opentrack.api.types"

## Index

### Type aliases

* [DataToken](_opentrack_api_types_.md#markdown-header-DataToken)
* [PhoneNumber](_opentrack_api_types_.md#markdown-header-PhoneNumber)

### Functions

* [isDataToken](_opentrack_api_types_.md#markdown-header-isDataToken)
* [isPhoneNumber](_opentrack_api_types_.md#markdown-header-isPhoneNumber)

---

## Type aliases

###  DataToken

**Ƭ DataToken**: *`string`*

*Defined in [opentrack.api.types.ts:3](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.types.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.types.ts-3)*

___

###  PhoneNumber

**Ƭ PhoneNumber**: *`string`*

*Defined in [opentrack.api.types.ts:12](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.types.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.types.ts-12)*

___

## Functions

###  isDataToken

▸ **isDataToken**(value: *`any`*): `boolean`

*Defined in [opentrack.api.types.ts:5](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.types.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.types.ts-5)*

**Parameters:**

| Name | Type |
| ------ | ------ |
| value | `any` |

**Returns:** `boolean`

___

###  isPhoneNumber

▸ **isPhoneNumber**(value: *`any`*): `boolean`

*Defined in [opentrack.api.types.ts:13](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.types.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.types.ts-13)*

**Parameters:**

| Name | Type |
| ------ | ------ |
| value | `any` |

**Returns:** `boolean`

___

