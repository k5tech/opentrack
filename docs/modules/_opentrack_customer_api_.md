[@k5tech/opentrack](../README.md) > ["opentrack.customer.api"](../modules/_opentrack_customer_api_.md)

# External module: "opentrack.customer.api"

## Index

### Classes

* [OpenTrackCustomerAPI](../classes/_opentrack_customer_api_.opentrackcustomerapi.md)

### Interfaces

* [Customer](../interfaces/_opentrack_customer_api_.customer.md)

### Type aliases

* [CustomerLookupParms](_opentrack_customer_api_.md#markdown-header-CustomerLookupParms)
* [CustomerSearchParms](_opentrack_customer_api_.md#markdown-header-CustomerSearchParms)

---

## Type aliases

###  CustomerLookupParms

**Ƭ CustomerLookupParms**: *`Partial`<`object`>*

*Defined in [opentrack.customer.api.ts:47](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.customer.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.customer.api.ts-47)*

___

###  CustomerSearchParms

**Ƭ CustomerSearchParms**: *`Partial`<`object`>*

*Defined in [opentrack.customer.api.ts:30](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.customer.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.customer.api.ts-30)*

___

