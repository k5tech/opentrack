[@k5tech/opentrack](../README.md) > ["opentrack.service.api"](../modules/_opentrack_service_api_.md)

# External module: "opentrack.service.api"

## Index

### Classes

* [OpenTrackServiceAPI](../classes/_opentrack_service_api_.opentrackserviceapi.md)

### Interfaces

* [ClosedRepairOrder](../interfaces/_opentrack_service_api_.closedrepairorder.md)
* [OpenRepairOrderLookupParms](../interfaces/_opentrack_service_api_.openrepairorderlookupparms.md)
* [RepairOrderLookupResult](../interfaces/_opentrack_service_api_.repairorderlookupresult.md)

### Type aliases

* [GetClosedRepairOrdersRequest](_opentrack_service_api_.md#markdown-header-GetClosedRepairOrdersRequest)

---

## Type aliases

###  GetClosedRepairOrdersRequest

**Ƭ GetClosedRepairOrdersRequest**: *`Partial`<`object`>*

*Defined in [opentrack.service.api.ts:7](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.service.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.service.api.ts-7)*

___

