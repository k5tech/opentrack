[@k5tech/opentrack](../README.md) > ["opentrack.api"](../modules/_opentrack_api_.md) > [APIListResult](../interfaces/_opentrack_api_.apilistresult.md)

# Interface: APIListResult

## Type parameters
#### T 
## Hierarchy

**APIListResult**

## Index

### Properties

* [Errors](_opentrack_api_.apilistresult.md#markdown-header-Optional-Errors)
* [Result](_opentrack_api_.apilistresult.md#markdown-header-Optional-Result)
* [Token](_opentrack_api_.apilistresult.md#markdown-header-Token)

---

## Properties

### `<Optional>` Errors

**● Errors**: *`object`*

*Defined in [opentrack.api.ts:9](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-9)*

#### Type declaration

 Error: [APIError](_opentrack_api_.apierror.md)[]

___

### `<Optional>` Result

**● Result**: *`T`[]*

*Defined in [opentrack.api.ts:12](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-12)*

___

###  Token

**● Token**: *[DataToken](../modules/_opentrack_api_types_.md#markdown-header-DataToken)*

*Defined in [opentrack.api.ts:8](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-8)*

___

