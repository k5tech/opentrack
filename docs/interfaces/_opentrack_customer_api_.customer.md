[@k5tech/opentrack](../README.md) > ["opentrack.customer.api"](../modules/_opentrack_customer_api_.md) > [Customer](../interfaces/_opentrack_customer_api_.customer.md)

# Interface: Customer

## Hierarchy

**Customer**

## Index

### Properties

* [Address1](_opentrack_customer_api_.customer.md#markdown-header-Address1)
* [Address2](_opentrack_customer_api_.customer.md#markdown-header-Address2)
* [Address3](_opentrack_customer_api_.customer.md#markdown-header-Address3)
* [BusinessPhone](_opentrack_customer_api_.customer.md#markdown-header-BusinessPhone)
* [BusinessPhoneExt](_opentrack_customer_api_.customer.md#markdown-header-BusinessPhoneExt)
* [CCCD](_opentrack_customer_api_.customer.md#markdown-header-CCCD)
* [CellPhone](_opentrack_customer_api_.customer.md#markdown-header-CellPhone)
* [City](_opentrack_customer_api_.customer.md#markdown-header-City)
* [CompanyNumber](_opentrack_customer_api_.customer.md#markdown-header-CompanyNumber)
* [Country](_opentrack_customer_api_.customer.md#markdown-header-Country)
* [CustomerNumber](_opentrack_customer_api_.customer.md#markdown-header-CustomerNumber)
* [Email1](_opentrack_customer_api_.customer.md#markdown-header-Email1)
* [Email2](_opentrack_customer_api_.customer.md#markdown-header-Email2)
* [EntityType](_opentrack_customer_api_.customer.md#markdown-header-EntityType)
* [FaxPhone](_opentrack_customer_api_.customer.md#markdown-header-FaxPhone)
* [FirstName](_opentrack_customer_api_.customer.md#markdown-header-FirstName)
* [HomePhone](_opentrack_customer_api_.customer.md#markdown-header-HomePhone)
* [LastName](_opentrack_customer_api_.customer.md#markdown-header-LastName)
* [MiddleName](_opentrack_customer_api_.customer.md#markdown-header-MiddleName)
* [Salutation](_opentrack_customer_api_.customer.md#markdown-header-Salutation)
* [State](_opentrack_customer_api_.customer.md#markdown-header-State)
* [Vehicles](_opentrack_customer_api_.customer.md#markdown-header-Vehicles)
* [ZipCode](_opentrack_customer_api_.customer.md#markdown-header-ZipCode)

---

## Properties

###  Address1

**● Address1**: *`string`*

*Defined in [opentrack.customer.api.ts:11](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.customer.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.customer.api.ts-11)*

___

###  Address2

**● Address2**: *`string`*

*Defined in [opentrack.customer.api.ts:12](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.customer.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.customer.api.ts-12)*

___

###  Address3

**● Address3**: *`string`*

*Defined in [opentrack.customer.api.ts:13](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.customer.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.customer.api.ts-13)*

___

###  BusinessPhone

**● BusinessPhone**: *[PhoneNumber](../modules/_opentrack_api_types_.md#markdown-header-PhoneNumber)*

*Defined in [opentrack.customer.api.ts:20](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.customer.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.customer.api.ts-20)*

___

###  BusinessPhoneExt

**● BusinessPhoneExt**: *`string`*

*Defined in [opentrack.customer.api.ts:21](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.customer.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.customer.api.ts-21)*

___

###  CCCD

**● CCCD**: *`number`*

*Defined in [opentrack.customer.api.ts:28](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.customer.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.customer.api.ts-28)*

___

###  CellPhone

**● CellPhone**: *[PhoneNumber](../modules/_opentrack_api_types_.md#markdown-header-PhoneNumber)*

*Defined in [opentrack.customer.api.ts:19](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.customer.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.customer.api.ts-19)*

___

###  City

**● City**: *`string`*

*Defined in [opentrack.customer.api.ts:14](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.customer.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.customer.api.ts-14)*

___

###  CompanyNumber

**● CompanyNumber**: *`string`*

*Defined in [opentrack.customer.api.ts:4](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.customer.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.customer.api.ts-4)*

___

###  Country

**● Country**: *`string`*

*Defined in [opentrack.customer.api.ts:17](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.customer.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.customer.api.ts-17)*

___

###  CustomerNumber

**● CustomerNumber**: *`string`*

*Defined in [opentrack.customer.api.ts:5](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.customer.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.customer.api.ts-5)*

___

###  Email1

**● Email1**: *`string`*

*Defined in [opentrack.customer.api.ts:23](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.customer.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.customer.api.ts-23)*

___

###  Email2

**● Email2**: *`string`*

*Defined in [opentrack.customer.api.ts:24](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.customer.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.customer.api.ts-24)*

___

###  EntityType

**● EntityType**: * "C" &#124; "I"
*

*Defined in [opentrack.customer.api.ts:6](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.customer.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.customer.api.ts-6)*

___

###  FaxPhone

**● FaxPhone**: *[PhoneNumber](../modules/_opentrack_api_types_.md#markdown-header-PhoneNumber)*

*Defined in [opentrack.customer.api.ts:22](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.customer.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.customer.api.ts-22)*

___

###  FirstName

**● FirstName**: *`string`*

*Defined in [opentrack.customer.api.ts:8](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.customer.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.customer.api.ts-8)*

___

###  HomePhone

**● HomePhone**: *[PhoneNumber](../modules/_opentrack_api_types_.md#markdown-header-PhoneNumber)*

*Defined in [opentrack.customer.api.ts:18](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.customer.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.customer.api.ts-18)*

___

###  LastName

**● LastName**: *`string`*

*Defined in [opentrack.customer.api.ts:7](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.customer.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.customer.api.ts-7)*

___

###  MiddleName

**● MiddleName**: *`string`*

*Defined in [opentrack.customer.api.ts:9](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.customer.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.customer.api.ts-9)*

___

###  Salutation

**● Salutation**: *`string`*

*Defined in [opentrack.customer.api.ts:10](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.customer.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.customer.api.ts-10)*

___

###  State

**● State**: *`string`*

*Defined in [opentrack.customer.api.ts:15](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.customer.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.customer.api.ts-15)*

___

###  Vehicles

**● Vehicles**: *`object`*

*Defined in [opentrack.customer.api.ts:25](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.customer.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.customer.api.ts-25)*

#### Type declaration

 VIN: `string`

___

###  ZipCode

**● ZipCode**: *`string`*

*Defined in [opentrack.customer.api.ts:16](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.customer.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.customer.api.ts-16)*

___

