[@k5tech/opentrack](../README.md) > ["opentrack.api"](../modules/_opentrack_api_.md) > [OpenTrackAPI](../interfaces/_opentrack_api_.opentrackapi.md)

# Interface: OpenTrackAPI

## Hierarchy

**OpenTrackAPI**

## Implemented by

* [OpenTrackAPIBase](../classes/_opentrack_api_.opentrackapibase.md)
* [OpenTrackCustomerAPI](../classes/_opentrack_customer_api_.opentrackcustomerapi.md)
* [OpenTrackPartsAPI](../classes/_opentrack_parts_api_.opentrackpartsapi.md)
* [OpenTrackServiceAPI](../classes/_opentrack_service_api_.opentrackserviceapi.md)

## Index

### Properties

* [endpoint](_opentrack_api_.opentrackapi.md#markdown-header-endpoint)

---

## Properties

###  endpoint

**● endpoint**: *`string`*

*Defined in [opentrack.api.ts:5](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-5)*

___

