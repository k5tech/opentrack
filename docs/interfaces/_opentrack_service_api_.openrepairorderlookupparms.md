[@k5tech/opentrack](../README.md) > ["opentrack.service.api"](../modules/_opentrack_service_api_.md) > [OpenRepairOrderLookupParms](../interfaces/_opentrack_service_api_.openrepairorderlookupparms.md)

# Interface: OpenRepairOrderLookupParms

## Hierarchy

**OpenRepairOrderLookupParms**

## Index

### Properties

* [CreatedDateTimeEnd](_opentrack_service_api_.openrepairorderlookupparms.md#markdown-header-CreatedDateTimeEnd)
* [CreatedDateTimeStart](_opentrack_service_api_.openrepairorderlookupparms.md#markdown-header-CreatedDateTimeStart)
* [CustomerName](_opentrack_service_api_.openrepairorderlookupparms.md#markdown-header-CustomerName)
* [CustomerNumber](_opentrack_service_api_.openrepairorderlookupparms.md#markdown-header-CustomerNumber)
* [InternalKey](_opentrack_service_api_.openrepairorderlookupparms.md#markdown-header-InternalKey)
* [InternalOnly](_opentrack_service_api_.openrepairorderlookupparms.md#markdown-header-InternalOnly)
* [ModifiedAfter](_opentrack_service_api_.openrepairorderlookupparms.md#markdown-header-ModifiedAfter)
* [RepairOrderNumber](_opentrack_service_api_.openrepairorderlookupparms.md#markdown-header-RepairOrderNumber)
* [StockNumber](_opentrack_service_api_.openrepairorderlookupparms.md#markdown-header-StockNumber)
* [TagNumber](_opentrack_service_api_.openrepairorderlookupparms.md#markdown-header-TagNumber)
* [VIN](_opentrack_service_api_.openrepairorderlookupparms.md#markdown-header-VIN)
* [VIN6](_opentrack_service_api_.openrepairorderlookupparms.md#markdown-header-VIN6)

---

## Properties

###  CreatedDateTimeEnd

**● CreatedDateTimeEnd**: *`string`*

*Defined in [opentrack.service.api.ts:23](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.service.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.service.api.ts-23)*

___

###  CreatedDateTimeStart

**● CreatedDateTimeStart**: *`string`*

*Defined in [opentrack.service.api.ts:24](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.service.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.service.api.ts-24)*

___

###  CustomerName

**● CustomerName**: *`string`*

*Defined in [opentrack.service.api.ts:21](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.service.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.service.api.ts-21)*

___

###  CustomerNumber

**● CustomerNumber**: *`string`*

*Defined in [opentrack.service.api.ts:22](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.service.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.service.api.ts-22)*

___

###  InternalKey

**● InternalKey**: *`string`*

*Defined in [opentrack.service.api.ts:25](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.service.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.service.api.ts-25)*

___

###  InternalOnly

**● InternalOnly**: *"Y"*

*Defined in [opentrack.service.api.ts:26](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.service.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.service.api.ts-26)*

___

###  ModifiedAfter

**● ModifiedAfter**: *`string`*

*Defined in [opentrack.service.api.ts:32](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.service.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.service.api.ts-32)*

___

###  RepairOrderNumber

**● RepairOrderNumber**: *`string`*

*Defined in [opentrack.service.api.ts:27](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.service.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.service.api.ts-27)*

___

###  StockNumber

**● StockNumber**: *`string`*

*Defined in [opentrack.service.api.ts:28](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.service.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.service.api.ts-28)*

___

###  TagNumber

**● TagNumber**: *`string`*

*Defined in [opentrack.service.api.ts:29](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.service.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.service.api.ts-29)*

___

###  VIN

**● VIN**: *`string`*

*Defined in [opentrack.service.api.ts:30](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.service.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.service.api.ts-30)*

___

###  VIN6

**● VIN6**: *`string`*

*Defined in [opentrack.service.api.ts:31](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.service.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.service.api.ts-31)*

___

