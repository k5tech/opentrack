[@k5tech/opentrack](../README.md) > ["opentrack.service.api"](../modules/_opentrack_service_api_.md) > [RepairOrderLookupResult](../interfaces/_opentrack_service_api_.repairorderlookupresult.md)

# Interface: RepairOrderLookupResult

## Hierarchy

**RepairOrderLookupResult**

## Index

### Properties

* [CompanyNumber](_opentrack_service_api_.repairorderlookupresult.md#markdown-header-CompanyNumber)
* [CustomerName](_opentrack_service_api_.repairorderlookupresult.md#markdown-header-CustomerName)
* [CustomerNumber](_opentrack_service_api_.repairorderlookupresult.md#markdown-header-CustomerNumber)
* [InternalKey](_opentrack_service_api_.repairorderlookupresult.md#markdown-header-InternalKey)
* [OpenTransactionDate](_opentrack_service_api_.repairorderlookupresult.md#markdown-header-OpenTransactionDate)
* [PartsTotal](_opentrack_service_api_.repairorderlookupresult.md#markdown-header-PartsTotal)
* [RepairOrderNumber](_opentrack_service_api_.repairorderlookupresult.md#markdown-header-RepairOrderNumber)
* [ShippingTotal](_opentrack_service_api_.repairorderlookupresult.md#markdown-header-ShippingTotal)
* [TotalEstimate](_opentrack_service_api_.repairorderlookupresult.md#markdown-header-TotalEstimate)

---

## Properties

###  CompanyNumber

**● CompanyNumber**: *`string`*

*Defined in [opentrack.service.api.ts:36](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.service.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.service.api.ts-36)*

___

###  CustomerName

**● CustomerName**: *`string`*

*Defined in [opentrack.service.api.ts:41](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.service.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.service.api.ts-41)*

___

###  CustomerNumber

**● CustomerNumber**: *`string`*

*Defined in [opentrack.service.api.ts:40](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.service.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.service.api.ts-40)*

___

###  InternalKey

**● InternalKey**: *`string`*

*Defined in [opentrack.service.api.ts:38](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.service.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.service.api.ts-38)*

___

###  OpenTransactionDate

**● OpenTransactionDate**: *`string`*

*Defined in [opentrack.service.api.ts:37](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.service.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.service.api.ts-37)*

___

###  PartsTotal

**● PartsTotal**: *`string`*

*Defined in [opentrack.service.api.ts:42](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.service.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.service.api.ts-42)*

___

###  RepairOrderNumber

**● RepairOrderNumber**: *`string`*

*Defined in [opentrack.service.api.ts:39](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.service.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.service.api.ts-39)*

___

###  ShippingTotal

**● ShippingTotal**: *`string`*

*Defined in [opentrack.service.api.ts:43](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.service.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.service.api.ts-43)*

___

###  TotalEstimate

**● TotalEstimate**: *`string`*

*Defined in [opentrack.service.api.ts:44](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.service.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.service.api.ts-44)*

___

