[@k5tech/opentrack](../README.md) > ["opentrack.api"](../modules/_opentrack_api_.md) > [APIAddResult](../interfaces/_opentrack_api_.apiaddresult.md)

# Interface: APIAddResult

## Hierarchy

**APIAddResult**

## Index

### Properties

* [Code](_opentrack_api_.apiaddresult.md#markdown-header-Code)
* [Message](_opentrack_api_.apiaddresult.md#markdown-header-Message)

---

## Properties

###  Code

**● Code**: *`string`*

*Defined in [opentrack.api.ts:21](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-21)*

___

###  Message

**● Message**: *`string`*

*Defined in [opentrack.api.ts:20](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-20)*

___

