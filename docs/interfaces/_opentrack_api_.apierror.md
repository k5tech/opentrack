[@k5tech/opentrack](../README.md) > ["opentrack.api"](../modules/_opentrack_api_.md) > [APIError](../interfaces/_opentrack_api_.apierror.md)

# Interface: APIError

## Hierarchy

**APIError**

## Index

### Properties

* [Code](_opentrack_api_.apierror.md#markdown-header-Code)
* [Message](_opentrack_api_.apierror.md#markdown-header-Message)

---

## Properties

###  Code

**● Code**: *`string`*

*Defined in [opentrack.api.ts:15](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-15)*

___

###  Message

**● Message**: *`string`*

*Defined in [opentrack.api.ts:16](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-16)*

___

