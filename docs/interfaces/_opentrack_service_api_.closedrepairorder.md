[@k5tech/opentrack](../README.md) > ["opentrack.service.api"](../modules/_opentrack_service_api_.md) > [ClosedRepairOrder](../interfaces/_opentrack_service_api_.closedrepairorder.md)

# Interface: ClosedRepairOrder

## Hierarchy

**ClosedRepairOrder**

## Index

### Properties

* [CompanyNumber](_opentrack_service_api_.closedrepairorder.md#markdown-header-CompanyNumber)
* [RepairOrderNumber](_opentrack_service_api_.closedrepairorder.md#markdown-header-RepairOrderNumber)

---

## Properties

###  CompanyNumber

**● CompanyNumber**: *`string`*

*Defined in [opentrack.service.api.ts:3](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.service.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.service.api.ts-3)*

___

###  RepairOrderNumber

**● RepairOrderNumber**: *`string`*

*Defined in [opentrack.service.api.ts:4](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.service.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.service.api.ts-4)*

___

