
OpenTrack API Wrapper
=====================

[![NPM Version](https://img.shields.io/npm/v/@k5tech/opentrack.svg)](https://npmjs.org/package/@k5tech/opentrack) [![Build Status](https://img.shields.io/bitbucket/pipelines/k5tech/opentrack.svg)](https://bitbucket.org/k5tech/opentrack/addon/pipelines/home) [![Coveralls Status](https://img.shields.io/coveralls/bitbucket/k5tech/opentrack.svg)](https://coveralls.io/bitbucket/k5tech/opentrack)

Installation
------------

```sh
npm install --save @k5tech/opentrack
```

Example
-------

```typescript
import { OpenTrackCustomerAPI } from '@k5tech/opentrack';

// ...

const api = (await OpenTrackCustomerAPI.create())
                .authenticatedAs(username, password)
                .dealer('AY', 'AY4', 'arkonap.arkona.com');

const apiResponse = await api.customerSearch({Phone: phoneNumber});

console.log(apiResponse);
```

### API Modules

#### Customer

Customer Search

```typescript
import { OpenTrackCustomerAPI } from '@k5tech/opentrack';

// ...

const api = await OpenTrackCustomerAPI.create();
const apiResponse = await api.customerSearch({ Phone: '5558369361' });

console.log(apiResponse);
```

Customer Lookup

```typescript
import { OpenTrackCustomerAPI } from '@k5tech/opentrack';

// ...

const api = await OpenTrackCustomerAPI.create();
const apiResponse = await api.customerLookup({ CustomerNumber: '1039358' });

console.log(apiResponse);
```

#### Service

Open Repair Order Lookup

```typescript
import { OpenTrackServiceAPI } from './opentrack.service.api';

// ...

const now = new Date('2017-11-08T22:02:51.000Z');
const start = new Date(now);
const end = new Date(now);
start.setDate(start.getDate() - 1);
end.setDate(end.getDate() + 1);

const api = (await OpenTrackServiceAPI.create());

const apiResponse = await api.openRepairOrderLookup({
    CreatedDateTimeEnd: end.toISOString(),
    CreatedDateTimeStart: start.toISOString(),
    InternalOnly: 'Y',
});

console.log(apiResponse);
```

## Index

### External modules

* ["index"](modules/_index_.md)
* ["opentrack.api"](modules/_opentrack_api_.md)
* ["opentrack.api.types"](modules/_opentrack_api_types_.md)
* ["opentrack.customer.api"](modules/_opentrack_customer_api_.md)
* ["opentrack.parts.api"](modules/_opentrack_parts_api_.md)
* ["opentrack.service.api"](modules/_opentrack_service_api_.md)

---

