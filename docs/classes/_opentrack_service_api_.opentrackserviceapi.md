[@k5tech/opentrack](../README.md) > ["opentrack.service.api"](../modules/_opentrack_service_api_.md) > [OpenTrackServiceAPI](../classes/_opentrack_service_api_.opentrackserviceapi.md)

# Class: OpenTrackServiceAPI

## Hierarchy

 [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md)

**↳ OpenTrackServiceAPI**

## Implements

* [OpenTrackAPI](../interfaces/_opentrack_api_.opentrackapi.md)

## Index

### Constructors

* [constructor](_opentrack_service_api_.opentrackserviceapi.md#markdown-header-constructor)

### Properties

* [endpoint](_opentrack_service_api_.opentrackserviceapi.md#markdown-header-endpoint)
* [port](_opentrack_service_api_.opentrackserviceapi.md#markdown-header-port)

### Accessors

* [wsdl](_opentrack_service_api_.opentrackserviceapi.md#markdown-header-wsdl)

### Methods

* [authenticatedAs](_opentrack_service_api_.opentrackserviceapi.md#markdown-header-authenticatedAs)
* [dealer](_opentrack_service_api_.opentrackserviceapi.md#markdown-header-dealer)
* [defaultParams](_opentrack_service_api_.opentrackserviceapi.md#markdown-header-defaultParams)
* [describe](_opentrack_service_api_.opentrackserviceapi.md#markdown-header-describe)
* [getApi](_opentrack_service_api_.opentrackserviceapi.md#markdown-header-getApi)
* [getClosedRepairOrders](_opentrack_service_api_.opentrackserviceapi.md#markdown-header-getClosedRepairOrders)
* [getErrorMessage](_opentrack_service_api_.opentrackserviceapi.md#markdown-header-getErrorMessage)
* [openRepairOrderLookup](_opentrack_service_api_.opentrackserviceapi.md#markdown-header-openRepairOrderLookup)
* [runApiMethod](_opentrack_service_api_.opentrackserviceapi.md#markdown-header-runApiMethod)
* [setupSoapClient](_opentrack_service_api_.opentrackserviceapi.md#markdown-header-setupSoapClient)
* [soapClient](_opentrack_service_api_.opentrackserviceapi.md#markdown-header-soapClient)
* [create](_opentrack_service_api_.opentrackserviceapi.md#markdown-header-Static-create)

---

## Constructors

###  constructor

⊕ **new OpenTrackServiceAPI**(endpoint?: *`string`*): [OpenTrackServiceAPI](_opentrack_service_api_.opentrackserviceapi.md)

*Overrides [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[constructor](_opentrack_api_.opentrackapibase.md#markdown-header-constructor)*

*Defined in [opentrack.service.api.ts:55](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.service.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.service.api.ts-55)*

**Parameters:**

| Name | Type | Default value |
| ------ | ------ | ------ |
| `Default value` endpoint | `string` | &quot;https://otstaging.arkona.com/opentrack/serviceapi.asmx&quot; |

**Returns:** [OpenTrackServiceAPI](_opentrack_service_api_.opentrackserviceapi.md)

___

## Properties

###  endpoint

**● endpoint**: *`string`*

*Implementation of [OpenTrackAPI](../interfaces/_opentrack_api_.opentrackapi.md).[endpoint](../interfaces/_opentrack_api_.opentrackapi.md#markdown-header-endpoint)*

*Inherited from [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[endpoint](_opentrack_api_.opentrackapibase.md#markdown-header-endpoint)*

*Defined in [opentrack.api.ts:40](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-40)*

___

###  port

**● port**: *`string`* = "ServiceApiSoap"

*Overrides [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[port](_opentrack_api_.opentrackapibase.md#markdown-header-port)*

*Defined in [opentrack.service.api.ts:49](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.service.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.service.api.ts-49)*

___

## Accessors

###  wsdl

getwsdl(): `string`

*Inherited from [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[wsdl](_opentrack_api_.opentrackapibase.md#markdown-header-wsdl)*

*Defined in [opentrack.api.ts:29](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-29)*

**Returns:** `string`

___

## Methods

###  authenticatedAs

▸ **authenticatedAs**(username: *`string`*, password: *`string`*, options?: *`any`*): `this`

*Inherited from [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[authenticatedAs](_opentrack_api_.opentrackapibase.md#markdown-header-authenticatedAs)*

*Defined in [opentrack.api.ts:91](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-91)*

**Parameters:**

| Name | Type |
| ------ | ------ |
| username | `string` |
| password | `string` |
| `Optional` options | `any` |

**Returns:** `this`

___

###  dealer

▸ **dealer**(enterpriseCode: *`string`*, companyNumber: *`string`*, serverName: *`string`*): `this`

*Inherited from [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[dealer](_opentrack_api_.opentrackapibase.md#markdown-header-dealer)*

*Defined in [opentrack.api.ts:53](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-53)*

**Parameters:**

| Name | Type |
| ------ | ------ |
| enterpriseCode | `string` |
| companyNumber | `string` |
| serverName | `string` |

**Returns:** `this`

___

###  defaultParams

▸ **defaultParams**(): `object`

*Inherited from [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[defaultParams](_opentrack_api_.opentrackapibase.md#markdown-header-defaultParams)*

*Defined in [opentrack.api.ts:62](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-62)*

**Returns:** `object`

___

###  describe

▸ **describe**(): `Promise`<`any`>

*Inherited from [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[describe](_opentrack_api_.opentrackapibase.md#markdown-header-describe)*

*Defined in [opentrack.api.ts:103](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-103)*

**Returns:** `Promise`<`any`>

___

###  getApi

▸ **getApi**(): `Promise`< `Function` &#124; `ISoapMethod` &#124; `WSDL`>

*Overrides [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[getApi](_opentrack_api_.opentrackapibase.md#markdown-header-getApi)*

*Defined in [opentrack.service.api.ts:69](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.service.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.service.api.ts-69)*

**Returns:** `Promise`< `Function` &#124; `ISoapMethod` &#124; `WSDL`>

___

###  getClosedRepairOrders

▸ **getClosedRepairOrders**(request: *[GetClosedRepairOrdersRequest](../modules/_opentrack_service_api_.md#markdown-header-GetClosedRepairOrdersRequest)*): `Promise`<[APIListResult](../interfaces/_opentrack_api_.apilistresult.md)<[ClosedRepairOrder](../interfaces/_opentrack_service_api_.closedrepairorder.md)>>

*Defined in [opentrack.service.api.ts:59](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.service.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.service.api.ts-59)*

**Parameters:**

| Name | Type |
| ------ | ------ |
| request | [GetClosedRepairOrdersRequest](../modules/_opentrack_service_api_.md#markdown-header-GetClosedRepairOrdersRequest) |

**Returns:** `Promise`<[APIListResult](../interfaces/_opentrack_api_.apilistresult.md)<[ClosedRepairOrder](../interfaces/_opentrack_service_api_.closedrepairorder.md)>>

___

###  getErrorMessage

▸ **getErrorMessage**(errObject: *`any`*): `any`

*Inherited from [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[getErrorMessage](_opentrack_api_.opentrackapibase.md#markdown-header-getErrorMessage)*

*Defined in [opentrack.api.ts:107](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-107)*

**Parameters:**

| Name | Type |
| ------ | ------ |
| errObject | `any` |

**Returns:** `any`

___

###  openRepairOrderLookup

▸ **openRepairOrderLookup**(lookupParams: *`Partial`<[OpenRepairOrderLookupParms](../interfaces/_opentrack_service_api_.openrepairorderlookupparms.md)>*): `Promise`<[APIListResult](../interfaces/_opentrack_api_.apilistresult.md)<[RepairOrderLookupResult](../interfaces/_opentrack_service_api_.repairorderlookupresult.md)>>

*Defined in [opentrack.service.api.ts:64](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.service.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.service.api.ts-64)*

**Parameters:**

| Name | Type |
| ------ | ------ |
| lookupParams | `Partial`<[OpenRepairOrderLookupParms](../interfaces/_opentrack_service_api_.openrepairorderlookupparms.md)> |

**Returns:** `Promise`<[APIListResult](../interfaces/_opentrack_api_.apilistresult.md)<[RepairOrderLookupResult](../interfaces/_opentrack_service_api_.repairorderlookupresult.md)>>

___

###  runApiMethod

▸ **runApiMethod**<`T`>(method: *`string`*, args?: *`any`*): `Promise`<`T`>

*Inherited from [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[runApiMethod](_opentrack_api_.opentrackapibase.md#markdown-header-runApiMethod)*

*Defined in [opentrack.api.ts:66](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-66)*

**Type parameters:**

#### T 
**Parameters:**

| Name | Type |
| ------ | ------ |
| method | `string` |
| `Optional` args | `any` |

**Returns:** `Promise`<`T`>

___

###  setupSoapClient

▸ **setupSoapClient**(username?: *`string`*, password?: *`string`*): `Promise`<`void`>

*Inherited from [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[setupSoapClient](_opentrack_api_.opentrackapibase.md#markdown-header-setupSoapClient)*

*Defined in [opentrack.api.ts:84](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-84)*

**Parameters:**

| Name | Type |
| ------ | ------ |
| `Optional` username | `string` |
| `Optional` password | `string` |

**Returns:** `Promise`<`void`>

___

###  soapClient

▸ **soapClient**(): `Promise`<`Client`>

*Inherited from [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[soapClient](_opentrack_api_.opentrackapibase.md#markdown-header-soapClient)*

*Defined in [opentrack.api.ts:96](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-96)*

**Returns:** `Promise`<`Client`>

___

### `<Static>` create

▸ **create**(endpoint?: *`string`*): `Promise`<[OpenTrackServiceAPI](_opentrack_service_api_.opentrackserviceapi.md)>

*Overrides [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[create](_opentrack_api_.opentrackapibase.md#markdown-header-Static-create)*

*Defined in [opentrack.service.api.ts:51](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.service.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.service.api.ts-51)*

**Parameters:**

| Name | Type | Default value |
| ------ | ------ | ------ |
| `Default value` endpoint | `string` | &quot;https://otstaging.arkona.com/opentrack/serviceapi.asmx&quot; |

**Returns:** `Promise`<[OpenTrackServiceAPI](_opentrack_service_api_.opentrackserviceapi.md)>

___

