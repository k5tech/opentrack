[@k5tech/opentrack](../README.md) > ["opentrack.customer.api"](../modules/_opentrack_customer_api_.md) > [OpenTrackCustomerAPI](../classes/_opentrack_customer_api_.opentrackcustomerapi.md)

# Class: OpenTrackCustomerAPI

## Hierarchy

 [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md)

**↳ OpenTrackCustomerAPI**

## Implements

* [OpenTrackAPI](../interfaces/_opentrack_api_.opentrackapi.md)

## Index

### Constructors

* [constructor](_opentrack_customer_api_.opentrackcustomerapi.md#markdown-header-constructor)

### Properties

* [endpoint](_opentrack_customer_api_.opentrackcustomerapi.md#markdown-header-endpoint)
* [port](_opentrack_customer_api_.opentrackcustomerapi.md#markdown-header-port)

### Accessors

* [wsdl](_opentrack_customer_api_.opentrackcustomerapi.md#markdown-header-wsdl)

### Methods

* [authenticatedAs](_opentrack_customer_api_.opentrackcustomerapi.md#markdown-header-authenticatedAs)
* [customerAdd](_opentrack_customer_api_.opentrackcustomerapi.md#markdown-header-customerAdd)
* [customerLookup](_opentrack_customer_api_.opentrackcustomerapi.md#markdown-header-customerLookup)
* [customerSearch](_opentrack_customer_api_.opentrackcustomerapi.md#markdown-header-customerSearch)
* [dealer](_opentrack_customer_api_.opentrackcustomerapi.md#markdown-header-dealer)
* [defaultParams](_opentrack_customer_api_.opentrackcustomerapi.md#markdown-header-defaultParams)
* [describe](_opentrack_customer_api_.opentrackcustomerapi.md#markdown-header-describe)
* [describeDetail](_opentrack_customer_api_.opentrackcustomerapi.md#markdown-header-describeDetail)
* [getApi](_opentrack_customer_api_.opentrackcustomerapi.md#markdown-header-getApi)
* [getErrorMessage](_opentrack_customer_api_.opentrackcustomerapi.md#markdown-header-getErrorMessage)
* [runApiMethod](_opentrack_customer_api_.opentrackcustomerapi.md#markdown-header-runApiMethod)
* [setupSoapClient](_opentrack_customer_api_.opentrackcustomerapi.md#markdown-header-setupSoapClient)
* [soapClient](_opentrack_customer_api_.opentrackcustomerapi.md#markdown-header-soapClient)
* [create](_opentrack_customer_api_.opentrackcustomerapi.md#markdown-header-Static-create)

---

## Constructors

###  constructor

⊕ **new OpenTrackCustomerAPI**(endpoint?: *`string`*): [OpenTrackCustomerAPI](_opentrack_customer_api_.opentrackcustomerapi.md)

*Overrides [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[constructor](_opentrack_api_.opentrackapibase.md#markdown-header-constructor)*

*Defined in [opentrack.customer.api.ts:60](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.customer.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.customer.api.ts-60)*

**Parameters:**

| Name | Type | Default value |
| ------ | ------ | ------ |
| `Default value` endpoint | `string` | &quot;https://otstaging.arkona.com/opentrack/customerapi.asmx&quot; |

**Returns:** [OpenTrackCustomerAPI](_opentrack_customer_api_.opentrackcustomerapi.md)

___

## Properties

###  endpoint

**● endpoint**: *`string`*

*Implementation of [OpenTrackAPI](../interfaces/_opentrack_api_.opentrackapi.md).[endpoint](../interfaces/_opentrack_api_.opentrackapi.md#markdown-header-endpoint)*

*Inherited from [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[endpoint](_opentrack_api_.opentrackapibase.md#markdown-header-endpoint)*

*Defined in [opentrack.api.ts:40](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-40)*

___

###  port

**● port**: *`string`* = "CustomerApiSoap"

*Overrides [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[port](_opentrack_api_.opentrackapibase.md#markdown-header-port)*

*Defined in [opentrack.customer.api.ts:54](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.customer.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.customer.api.ts-54)*

___

## Accessors

###  wsdl

getwsdl(): `string`

*Inherited from [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[wsdl](_opentrack_api_.opentrackapibase.md#markdown-header-wsdl)*

*Defined in [opentrack.api.ts:29](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-29)*

**Returns:** `string`

___

## Methods

###  authenticatedAs

▸ **authenticatedAs**(username: *`string`*, password: *`string`*, options?: *`any`*): `this`

*Inherited from [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[authenticatedAs](_opentrack_api_.opentrackapibase.md#markdown-header-authenticatedAs)*

*Defined in [opentrack.api.ts:91](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-91)*

**Parameters:**

| Name | Type |
| ------ | ------ |
| username | `string` |
| password | `string` |
| `Optional` options | `any` |

**Returns:** `this`

___

###  customerAdd

▸ **customerAdd**(dataToken: *[DataToken](../modules/_opentrack_api_types_.md#markdown-header-DataToken)*, customer: *`Partial`<[Customer](../interfaces/_opentrack_customer_api_.customer.md)>*): `Promise`<[APIAddResult](../interfaces/_opentrack_api_.apiaddresult.md)>

*Defined in [opentrack.customer.api.ts:84](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.customer.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.customer.api.ts-84)*

**Parameters:**

| Name | Type |
| ------ | ------ |
| dataToken | [DataToken](../modules/_opentrack_api_types_.md#markdown-header-DataToken) |
| customer | `Partial`<[Customer](../interfaces/_opentrack_customer_api_.customer.md)> |

**Returns:** `Promise`<[APIAddResult](../interfaces/_opentrack_api_.apiaddresult.md)>

___

###  customerLookup

▸ **customerLookup**(lookupParams: *[CustomerLookupParms](../modules/_opentrack_customer_api_.md#markdown-header-CustomerLookupParms)*): `Promise`<[APIListResult](../interfaces/_opentrack_api_.apilistresult.md)<[Customer](../interfaces/_opentrack_customer_api_.customer.md)>>

*Defined in [opentrack.customer.api.ts:79](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.customer.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.customer.api.ts-79)*

**Parameters:**

| Name | Type |
| ------ | ------ |
| lookupParams | [CustomerLookupParms](../modules/_opentrack_customer_api_.md#markdown-header-CustomerLookupParms) |

**Returns:** `Promise`<[APIListResult](../interfaces/_opentrack_api_.apilistresult.md)<[Customer](../interfaces/_opentrack_customer_api_.customer.md)>>

___

###  customerSearch

▸ **customerSearch**(searchParams: *[CustomerSearchParms](../modules/_opentrack_customer_api_.md#markdown-header-CustomerSearchParms)*): `Promise`<[APIListResult](../interfaces/_opentrack_api_.apilistresult.md)<[Customer](../interfaces/_opentrack_customer_api_.customer.md)>>

*Defined in [opentrack.customer.api.ts:67](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.customer.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.customer.api.ts-67)*

**Parameters:**

| Name | Type |
| ------ | ------ |
| searchParams | [CustomerSearchParms](../modules/_opentrack_customer_api_.md#markdown-header-CustomerSearchParms) |

**Returns:** `Promise`<[APIListResult](../interfaces/_opentrack_api_.apilistresult.md)<[Customer](../interfaces/_opentrack_customer_api_.customer.md)>>

___

###  dealer

▸ **dealer**(enterpriseCode: *`string`*, companyNumber: *`string`*, serverName: *`string`*): `this`

*Inherited from [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[dealer](_opentrack_api_.opentrackapibase.md#markdown-header-dealer)*

*Defined in [opentrack.api.ts:53](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-53)*

**Parameters:**

| Name | Type |
| ------ | ------ |
| enterpriseCode | `string` |
| companyNumber | `string` |
| serverName | `string` |

**Returns:** `this`

___

###  defaultParams

▸ **defaultParams**(): `object`

*Inherited from [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[defaultParams](_opentrack_api_.opentrackapibase.md#markdown-header-defaultParams)*

*Defined in [opentrack.api.ts:62](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-62)*

**Returns:** `object`

___

###  describe

▸ **describe**(): `Promise`<`any`>

*Inherited from [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[describe](_opentrack_api_.opentrackapibase.md#markdown-header-describe)*

*Defined in [opentrack.api.ts:103](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-103)*

**Returns:** `Promise`<`any`>

___

###  describeDetail

▸ **describeDetail**(): `Promise`<`any`>

*Defined in [opentrack.customer.api.ts:93](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.customer.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.customer.api.ts-93)*

**Returns:** `Promise`<`any`>

___

###  getApi

▸ **getApi**(): `Promise`< `Function` &#124; `ISoapMethod` &#124; `WSDL`>

*Overrides [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[getApi](_opentrack_api_.opentrackapibase.md#markdown-header-getApi)*

*Defined in [opentrack.customer.api.ts:89](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.customer.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.customer.api.ts-89)*

**Returns:** `Promise`< `Function` &#124; `ISoapMethod` &#124; `WSDL`>

___

###  getErrorMessage

▸ **getErrorMessage**(errObject: *`any`*): `any`

*Inherited from [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[getErrorMessage](_opentrack_api_.opentrackapibase.md#markdown-header-getErrorMessage)*

*Defined in [opentrack.api.ts:107](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-107)*

**Parameters:**

| Name | Type |
| ------ | ------ |
| errObject | `any` |

**Returns:** `any`

___

###  runApiMethod

▸ **runApiMethod**<`T`>(method: *`string`*, args?: *`any`*): `Promise`<`T`>

*Inherited from [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[runApiMethod](_opentrack_api_.opentrackapibase.md#markdown-header-runApiMethod)*

*Defined in [opentrack.api.ts:66](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-66)*

**Type parameters:**

#### T 
**Parameters:**

| Name | Type |
| ------ | ------ |
| method | `string` |
| `Optional` args | `any` |

**Returns:** `Promise`<`T`>

___

###  setupSoapClient

▸ **setupSoapClient**(username?: *`string`*, password?: *`string`*): `Promise`<`void`>

*Inherited from [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[setupSoapClient](_opentrack_api_.opentrackapibase.md#markdown-header-setupSoapClient)*

*Defined in [opentrack.api.ts:84](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-84)*

**Parameters:**

| Name | Type |
| ------ | ------ |
| `Optional` username | `string` |
| `Optional` password | `string` |

**Returns:** `Promise`<`void`>

___

###  soapClient

▸ **soapClient**(): `Promise`<`Client`>

*Inherited from [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[soapClient](_opentrack_api_.opentrackapibase.md#markdown-header-soapClient)*

*Defined in [opentrack.api.ts:96](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-96)*

**Returns:** `Promise`<`Client`>

___

### `<Static>` create

▸ **create**(endpoint?: *`string`*): `Promise`<[OpenTrackCustomerAPI](_opentrack_customer_api_.opentrackcustomerapi.md)>

*Overrides [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[create](_opentrack_api_.opentrackapibase.md#markdown-header-Static-create)*

*Defined in [opentrack.customer.api.ts:56](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.customer.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.customer.api.ts-56)*

**Parameters:**

| Name | Type | Default value |
| ------ | ------ | ------ |
| `Default value` endpoint | `string` | &quot;https://otstaging.arkona.com/opentrack/customerapi.asmx&quot; |

**Returns:** `Promise`<[OpenTrackCustomerAPI](_opentrack_customer_api_.opentrackcustomerapi.md)>

___

