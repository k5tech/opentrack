[@k5tech/opentrack](../README.md) > ["opentrack.api"](../modules/_opentrack_api_.md) > [OpenTrackAPIBase](../classes/_opentrack_api_.opentrackapibase.md)

# Class: OpenTrackAPIBase

## Hierarchy

**OpenTrackAPIBase**

↳  [OpenTrackCustomerAPI](_opentrack_customer_api_.opentrackcustomerapi.md)

↳  [OpenTrackPartsAPI](_opentrack_parts_api_.opentrackpartsapi.md)

↳  [OpenTrackServiceAPI](_opentrack_service_api_.opentrackserviceapi.md)

## Implements

* [OpenTrackAPI](../interfaces/_opentrack_api_.opentrackapi.md)

## Index

### Constructors

* [constructor](_opentrack_api_.opentrackapibase.md#markdown-header-constructor)

### Properties

* [endpoint](_opentrack_api_.opentrackapibase.md#markdown-header-endpoint)
* [port](_opentrack_api_.opentrackapibase.md#markdown-header-port)
* [soapInstance](_opentrack_api_.opentrackapibase.md#markdown-header-Private-soapInstance)

### Accessors

* [wsdl](_opentrack_api_.opentrackapibase.md#markdown-header-wsdl)

### Methods

* [authenticatedAs](_opentrack_api_.opentrackapibase.md#markdown-header-authenticatedAs)
* [dealer](_opentrack_api_.opentrackapibase.md#markdown-header-dealer)
* [defaultParams](_opentrack_api_.opentrackapibase.md#markdown-header-defaultParams)
* [describe](_opentrack_api_.opentrackapibase.md#markdown-header-describe)
* [getApi](_opentrack_api_.opentrackapibase.md#markdown-header-getApi)
* [getErrorMessage](_opentrack_api_.opentrackapibase.md#markdown-header-getErrorMessage)
* [runApiMethod](_opentrack_api_.opentrackapibase.md#markdown-header-runApiMethod)
* [setupSoapClient](_opentrack_api_.opentrackapibase.md#markdown-header-setupSoapClient)
* [soapClient](_opentrack_api_.opentrackapibase.md#markdown-header-soapClient)
* [create](_opentrack_api_.opentrackapibase.md#markdown-header-Static-create)

### Object literals

* [requestParams](_opentrack_api_.opentrackapibase.md#markdown-header-object-literal-Private-requestParams)

---

## Constructors

###  constructor

⊕ **new OpenTrackAPIBase**(endpoint?: *`string`*): [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md)

*Defined in [opentrack.api.ts:38](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-38)*

**Parameters:**

| Name | Type | Default value |
| ------ | ------ | ------ |
| `Default value` endpoint | `string` | &quot;&quot; |

**Returns:** [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md)

___

## Properties

###  endpoint

**● endpoint**: *`string`*

*Implementation of [OpenTrackAPI](../interfaces/_opentrack_api_.opentrackapi.md).[endpoint](../interfaces/_opentrack_api_.opentrackapi.md#markdown-header-endpoint)*

*Defined in [opentrack.api.ts:40](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-40)*

___

###  port

**● port**: *`string`*

*Defined in [opentrack.api.ts:38](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-38)*

___

### `<Private>` soapInstance

**● soapInstance**: *`Client`*

*Defined in [opentrack.api.ts:33](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-33)*

___

## Accessors

###  wsdl

getwsdl(): `string`

*Defined in [opentrack.api.ts:29](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-29)*

**Returns:** `string`

___

## Methods

###  authenticatedAs

▸ **authenticatedAs**(username: *`string`*, password: *`string`*, options?: *`any`*): `this`

*Defined in [opentrack.api.ts:91](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-91)*

**Parameters:**

| Name | Type |
| ------ | ------ |
| username | `string` |
| password | `string` |
| `Optional` options | `any` |

**Returns:** `this`

___

###  dealer

▸ **dealer**(enterpriseCode: *`string`*, companyNumber: *`string`*, serverName: *`string`*): `this`

*Defined in [opentrack.api.ts:53](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-53)*

**Parameters:**

| Name | Type |
| ------ | ------ |
| enterpriseCode | `string` |
| companyNumber | `string` |
| serverName | `string` |

**Returns:** `this`

___

###  defaultParams

▸ **defaultParams**(): `object`

*Defined in [opentrack.api.ts:62](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-62)*

**Returns:** `object`

___

###  describe

▸ **describe**(): `Promise`<`any`>

*Defined in [opentrack.api.ts:103](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-103)*

**Returns:** `Promise`<`any`>

___

###  getApi

▸ **getApi**(): `Promise`< `ISoapMethod` &#124; `WSDL` &#124; `Function`>

*Defined in [opentrack.api.ts:49](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-49)*

**Returns:** `Promise`< `ISoapMethod` &#124; `WSDL` &#124; `Function`>

___

###  getErrorMessage

▸ **getErrorMessage**(errObject: *`any`*): `any`

*Defined in [opentrack.api.ts:107](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-107)*

**Parameters:**

| Name | Type |
| ------ | ------ |
| errObject | `any` |

**Returns:** `any`

___

###  runApiMethod

▸ **runApiMethod**<`T`>(method: *`string`*, args?: *`any`*): `Promise`<`T`>

*Defined in [opentrack.api.ts:66](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-66)*

**Type parameters:**

#### T 
**Parameters:**

| Name | Type |
| ------ | ------ |
| method | `string` |
| `Optional` args | `any` |

**Returns:** `Promise`<`T`>

___

###  setupSoapClient

▸ **setupSoapClient**(username?: *`string`*, password?: *`string`*): `Promise`<`void`>

*Defined in [opentrack.api.ts:84](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-84)*

**Parameters:**

| Name | Type |
| ------ | ------ |
| `Optional` username | `string` |
| `Optional` password | `string` |

**Returns:** `Promise`<`void`>

___

###  soapClient

▸ **soapClient**(): `Promise`<`Client`>

*Defined in [opentrack.api.ts:96](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-96)*

**Returns:** `Promise`<`Client`>

___

### `<Static>` create

▸ **create**(endpoint?: *`string`*): `Promise`<`any`>

*Defined in [opentrack.api.ts:43](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-43)*

**Parameters:**

| Name | Type | Default value |
| ------ | ------ | ------ |
| `Default value` endpoint | `string` | &quot;&quot; |

**Returns:** `Promise`<`any`>

___

## Object literals

### `<Private>` requestParams

**requestParams**: *`object`*

*Defined in [opentrack.api.ts:34](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-34)*

####  Dealer

**● Dealer**: *`null`* =  null

*Defined in [opentrack.api.ts:35](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-35)*

___

___

