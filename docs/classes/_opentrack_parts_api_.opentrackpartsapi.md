[@k5tech/opentrack](../README.md) > ["opentrack.parts.api"](../modules/_opentrack_parts_api_.md) > [OpenTrackPartsAPI](../classes/_opentrack_parts_api_.opentrackpartsapi.md)

# Class: OpenTrackPartsAPI

## Hierarchy

 [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md)

**↳ OpenTrackPartsAPI**

## Implements

* [OpenTrackAPI](../interfaces/_opentrack_api_.opentrackapi.md)

## Index

### Constructors

* [constructor](_opentrack_parts_api_.opentrackpartsapi.md#markdown-header-constructor)

### Properties

* [endpoint](_opentrack_parts_api_.opentrackpartsapi.md#markdown-header-endpoint)
* [port](_opentrack_parts_api_.opentrackpartsapi.md#markdown-header-port)

### Accessors

* [wsdl](_opentrack_parts_api_.opentrackpartsapi.md#markdown-header-wsdl)

### Methods

* [authenticatedAs](_opentrack_parts_api_.opentrackpartsapi.md#markdown-header-authenticatedAs)
* [dealer](_opentrack_parts_api_.opentrackpartsapi.md#markdown-header-dealer)
* [defaultParams](_opentrack_parts_api_.opentrackpartsapi.md#markdown-header-defaultParams)
* [describe](_opentrack_parts_api_.opentrackpartsapi.md#markdown-header-describe)
* [getApi](_opentrack_parts_api_.opentrackpartsapi.md#markdown-header-getApi)
* [getErrorMessage](_opentrack_parts_api_.opentrackpartsapi.md#markdown-header-getErrorMessage)
* [runApiMethod](_opentrack_parts_api_.opentrackpartsapi.md#markdown-header-runApiMethod)
* [setupSoapClient](_opentrack_parts_api_.opentrackpartsapi.md#markdown-header-setupSoapClient)
* [soapClient](_opentrack_parts_api_.opentrackpartsapi.md#markdown-header-soapClient)
* [create](_opentrack_parts_api_.opentrackpartsapi.md#markdown-header-Static-create)

---

## Constructors

###  constructor

⊕ **new OpenTrackPartsAPI**(endpoint?: *`string`*): [OpenTrackPartsAPI](_opentrack_parts_api_.opentrackpartsapi.md)

*Inherited from [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[constructor](_opentrack_api_.opentrackapibase.md#markdown-header-constructor)*

*Defined in [opentrack.api.ts:38](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-38)*

**Parameters:**

| Name | Type | Default value |
| ------ | ------ | ------ |
| `Default value` endpoint | `string` | &quot;&quot; |

**Returns:** [OpenTrackPartsAPI](_opentrack_parts_api_.opentrackpartsapi.md)

___

## Properties

###  endpoint

**● endpoint**: *`string`*

*Implementation of [OpenTrackAPI](../interfaces/_opentrack_api_.opentrackapi.md).[endpoint](../interfaces/_opentrack_api_.opentrackapi.md#markdown-header-endpoint)*

*Inherited from [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[endpoint](_opentrack_api_.opentrackapibase.md#markdown-header-endpoint)*

*Defined in [opentrack.api.ts:40](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-40)*

___

###  port

**● port**: *`string`*

*Inherited from [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[port](_opentrack_api_.opentrackapibase.md#markdown-header-port)*

*Defined in [opentrack.api.ts:38](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-38)*

___

## Accessors

###  wsdl

getwsdl(): `string`

*Inherited from [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[wsdl](_opentrack_api_.opentrackapibase.md#markdown-header-wsdl)*

*Defined in [opentrack.api.ts:29](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-29)*

**Returns:** `string`

___

## Methods

###  authenticatedAs

▸ **authenticatedAs**(username: *`string`*, password: *`string`*, options?: *`any`*): `this`

*Inherited from [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[authenticatedAs](_opentrack_api_.opentrackapibase.md#markdown-header-authenticatedAs)*

*Defined in [opentrack.api.ts:91](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-91)*

**Parameters:**

| Name | Type |
| ------ | ------ |
| username | `string` |
| password | `string` |
| `Optional` options | `any` |

**Returns:** `this`

___

###  dealer

▸ **dealer**(enterpriseCode: *`string`*, companyNumber: *`string`*, serverName: *`string`*): `this`

*Inherited from [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[dealer](_opentrack_api_.opentrackapibase.md#markdown-header-dealer)*

*Defined in [opentrack.api.ts:53](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-53)*

**Parameters:**

| Name | Type |
| ------ | ------ |
| enterpriseCode | `string` |
| companyNumber | `string` |
| serverName | `string` |

**Returns:** `this`

___

###  defaultParams

▸ **defaultParams**(): `object`

*Inherited from [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[defaultParams](_opentrack_api_.opentrackapibase.md#markdown-header-defaultParams)*

*Defined in [opentrack.api.ts:62](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-62)*

**Returns:** `object`

___

###  describe

▸ **describe**(): `Promise`<`any`>

*Inherited from [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[describe](_opentrack_api_.opentrackapibase.md#markdown-header-describe)*

*Defined in [opentrack.api.ts:103](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-103)*

**Returns:** `Promise`<`any`>

___

###  getApi

▸ **getApi**(): `Promise`< `Function` &#124; `ISoapMethod` &#124; `WSDL`>

*Overrides [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[getApi](_opentrack_api_.opentrackapibase.md#markdown-header-getApi)*

*Defined in [opentrack.parts.api.ts:3](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.parts.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.parts.api.ts-3)*

**Returns:** `Promise`< `Function` &#124; `ISoapMethod` &#124; `WSDL`>

___

###  getErrorMessage

▸ **getErrorMessage**(errObject: *`any`*): `any`

*Inherited from [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[getErrorMessage](_opentrack_api_.opentrackapibase.md#markdown-header-getErrorMessage)*

*Defined in [opentrack.api.ts:107](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-107)*

**Parameters:**

| Name | Type |
| ------ | ------ |
| errObject | `any` |

**Returns:** `any`

___

###  runApiMethod

▸ **runApiMethod**<`T`>(method: *`string`*, args?: *`any`*): `Promise`<`T`>

*Inherited from [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[runApiMethod](_opentrack_api_.opentrackapibase.md#markdown-header-runApiMethod)*

*Defined in [opentrack.api.ts:66](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-66)*

**Type parameters:**

#### T 
**Parameters:**

| Name | Type |
| ------ | ------ |
| method | `string` |
| `Optional` args | `any` |

**Returns:** `Promise`<`T`>

___

###  setupSoapClient

▸ **setupSoapClient**(username?: *`string`*, password?: *`string`*): `Promise`<`void`>

*Inherited from [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[setupSoapClient](_opentrack_api_.opentrackapibase.md#markdown-header-setupSoapClient)*

*Defined in [opentrack.api.ts:84](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-84)*

**Parameters:**

| Name | Type |
| ------ | ------ |
| `Optional` username | `string` |
| `Optional` password | `string` |

**Returns:** `Promise`<`void`>

___

###  soapClient

▸ **soapClient**(): `Promise`<`Client`>

*Inherited from [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[soapClient](_opentrack_api_.opentrackapibase.md#markdown-header-soapClient)*

*Defined in [opentrack.api.ts:96](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-96)*

**Returns:** `Promise`<`Client`>

___

### `<Static>` create

▸ **create**(endpoint?: *`string`*): `Promise`<`any`>

*Inherited from [OpenTrackAPIBase](_opentrack_api_.opentrackapibase.md).[create](_opentrack_api_.opentrackapibase.md#markdown-header-Static-create)*

*Defined in [opentrack.api.ts:43](https://bitbucket.org/k5tech/opentrack/src/master/opentrack.api.ts?fileviewer&amp;#x3D;file-view-default#opentrack.api.ts-43)*

**Parameters:**

| Name | Type | Default value |
| ------ | ------ | ------ |
| `Default value` endpoint | `string` | &quot;&quot; |

**Returns:** `Promise`<`any`>

___

